ISMI Team 5 - VerSe 2019
-----------------------------

This notebook contains the code to train and evaluate an [EfficientNet](https://arxiv.org/abs/1905.11946) based model.
Our model is defined in the VertebraeLocalizer class and can be adjusted by using several parameters.

 - `link_conf` enables the option to use the confidence as the input for the coordinate prediction
 - `num_skipconnections` defines the number of skip connections to be added
 
The notebook prepares two different dataset with a 80/10/10 train/test/val split. One is using affine transformations to augment the data.

_The current state of this notebook does not contain the output of our training_ since we have done this outside Jupyter. Our initial testing however was done using this notebook with 10 epochs instead of the 300 used during training.

The output of the 300 epoch training can be found in the figures below. These models were trained on a Google Cloud instance with a Tesla P100 GPU with a maximum epoch length of 10 seconds for the b6 networks. A trained b6 model is available [here](https://gitlab.science.ru.nl/ismi5/project/-/blob/model/efficientnet-b6-aug.zip).

| Model | Loss | Score | Id-rate | Inverted distance 
| - | - | - | - | - |
efficientnet-b1          | 2.268904 | 0.40989 |  0.359069 |  0.511532
efficientnet-b4          |  1.683857 |  0.423611 |  0.363460 |  0.543915
efficientnet-b6          |  1.394953 |  0.402621 |  0.338440 |  0.530985
efficientnet-b1-conf     |  2.357249 |  0.361013 |  0.300960 |  0.481119
efficientnet-b4-conf     |  1.734579 |  0.396169 |  0.336499 |  0.515511
efficientnet-b6-conf     |  1.309693 |  0.380221 |  0.312806 |  0.515053
efficientnet-b1-aug      |  0.768137 |  0.584075 |  0.496586 |  0.759054
efficientnet-b4-aug      |  0.926808 |  0.419602 |  0.343546 |  0.571715
efficientnet-b6-aug      |  0.786854 |  0.563053 |  0.491217 |  0.706728
efficientnet-b1-aug-conf |  0.799485 |  0.538238 |  0.499037 |  0.616640
efficientnet-b4-aug-conf |  0.885503 |  0.457083 |  0.396446 |  0.578357
efficientnet-b6-aug-conf |  0.719172 |  0.514436 |  0.445159 |  0.652992

![PII48QExNT7w9SF6mNbjsVERFAp4xERMRNgSAiIoACQURE3BQIIiICKBBERMRNgSAiIoACQURE3BQIIiICwP8Deg65MgmiWA4AAA.png](attachment:PII48QExNT7w9SF6mNbjsVERFAp4xERMRNgSAiIoACQURE3BQIIiICKBBERMRNgSAiIoACQURE3BQIIiICwP8Deg65MgmiWA4AAA.png)


## How to use

You should have received this notebook with a `requirements.txt` and the `wfs` library. Both are required to run every piece of code in this notebook. Place the `wfs` package in the same directory as this notebook to use it and use `pip install -r requirements.txt` to install all the required parameters. Note that the minimal Python version for this notebook is supposed to be *Python 3.6*. Even though we have mostly done training using Python 3.7.

We advise you to create a virtual env using `python3 -m venv venv` and using that environment to not install all the libraries globally.

Then last you should adjust the `DATA_DIR` variable to be able to read all the images and save the information as numpy file before starting training.

To just use a trained model to do predictions we have also added the `predict.py` script.

## Preprocessing

The images used in the model are [Maximum Intensity Projections](https://en.wikipedia.org/wiki/Maximum_intensity_projection) of the XY and XZ planes of the input 3D images. We apply contrast stretching, denoising and resizing (256 x 128 x 128) to normalise the input images.

## Evaluation

The evaluation measures that are calculated are based on the measures introduced in [Sekuboyina, Anjany, et al. "VerSe: A Vertebrae Labelling and Segmentation Benchmark."](https://arxiv.org/abs/2001.09193) that were also used in the original VerSe 2019 Grand-Challenge. It is not 100% but we have tried to use similar measures.

 - Identification rate
 - Localisation distance
   - Normalised and inversed
 - Score: 2/3 x identification rate + 1/3 distance