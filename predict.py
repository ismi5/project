import argparse
import json
import os
import time
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import SimpleITK as sitk

from skimage import restoration
from scipy.ndimage import zoom


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
from tensorflow.keras.models import load_model


tf.get_logger().setLevel('ERROR')


IMAGE_SHAPE = (128, 128, 256)

# Constrast-stretching parameters
CUTOFF_LOWER = 0
CUTOFF_UPPER = 2000

# Denoising parameters
DENOISE_METHOD = 'chambolle'
DENOISE_STRENGTH = 0.02

# C1 - L6
ANNOTATIONS = [f'C{i}' for i in range(1, 8)] + [f'Th{i}' for i in range(1, 13)] + [f'L{i}' for i in range(1, 7)]


def plot_prediction(image, predictions, destination, threshold=0.5):
    # Style parameters
    kwargs_image = dict(cmap=sns.cm.rocket, aspect='auto')
    kwargs_label_pred = dict(c='springgreen', marker='x', s=12)
    kwargs_annotation_pred = dict(c='green', fontsize=12, fontweight='bold')

    annotations = []

    # Select labels with confidence above the threshold
    label_xyz_pred = []
    for i, label in enumerate(predictions):
        if label[0] > threshold:
            label_xyz_pred.append(label[1:])
            annotations.append(ANNOTATIONS[i])
    label_xyz_pred = np.array(label_xyz_pred)

    fig, axes = plt.subplots(2, 1, sharex='all', sharey='all', gridspec_kw=dict(wspace=0))

    # Plot X-Y slice of data
    axes[0].imshow(image.max(axis=0), **kwargs_image)
    for (x, y, txt) in zip(label_xyz_pred[:, 2], label_xyz_pred[:, 1], annotations):
        axes[0].scatter(x, y, **kwargs_label_pred)
        axes[0].text(x, y - 10, txt, **kwargs_annotation_pred)
    axes[0].axis('off')
    axes[0].set_title('X/Y maximum intensity projection')

    # Plot X-Z slice of data
    axes[1].imshow(image.max(axis=1), **kwargs_image)
    for (x, z, txt) in zip(label_xyz_pred[:, 2], label_xyz_pred[:, 0], annotations):
        axes[1].scatter(x, z, **kwargs_label_pred)
        axes[1].text(x, z - 10, txt, **kwargs_annotation_pred)
    axes[1].axis('off')
    axes[1].set_title('X/Z maximum intensity projection')

    fig.subplots_adjust(hspace=0)
    fig.tight_layout()

    print(f'[*] Saving prediction plot to {destination}')
    plt.savefig(destination)

    plt.close(fig)


def prediction_to_json(prediction, destination, threshold=0.5):
    prediction_json = []

    for label, (conf, z, y, x) in enumerate(prediction):
        if conf > threshold:
            prediction_json.append({
                'X': x.item(),
                'Y': y.item(),
                'Z': z.item(),
                'label': label + 1,
            })

    print(f'[*] Saving prediction to {destination}')
    with open(destination, 'w') as _file:
        json.dump(prediction_json, _file)


def contrast_stretching(image, p0, pk, q0=None, qk=None):
    if q0 is None:
        q0 = p0

    if qk is None:
        qk = pk

    return np.clip(q0 + (qk - q0) * (image - p0) / (pk - p0), q0, qk)


def denoise(image, method, strength):
    if method == 'chambolle':
        return restoration.denoise_tv_chambolle(image, strength)

    if method == 'bregman':
        return restoration.denoise_tv_bregman(image, 1 / strength)

    raise NotImplementedError('Denoise method `%s` is not implemented.')


def load_image(path):
    # Load image
    file_image = sitk.ReadImage(path)

    # Build inverse affine from file
    affine = np.linalg.inv(np.array(file_image.GetDirection()).reshape(3, 3))

    # Extract axes swaps
    swaps = np.argmax(np.abs(affine), axis=0).astype(np.int)

    # Extract axes flips and correct them
    flips = np.sum(affine.round(), axis=0).astype(np.int)
    flips[2] *= -1

    # Extract spacings from file and correct them
    spacings = np.array(file_image.GetSpacing())[swaps]

    # Read image data from file
    image = sitk.GetArrayViewFromImage(file_image).astype(np.float32)

    # Swap axes to standard orientation (zyx)
    image = np.transpose(np.transpose(image, (2, 1, 0)), swaps)

    # Flip axes to standard directions
    image = image[tuple(slice(None, None, int(f)) for f in flips)]

    return image, spacings


def preprocess(image):
    # Calculate scaling ratios
    scale_ratios_image = np.divide(IMAGE_SHAPE, image.shape)

    # Resize image
    image = zoom(image, scale_ratios_image)

    # Constrast stretching
    image = contrast_stretching(image, CUTOFF_LOWER, CUTOFF_UPPER, 0, 1)

    # Denoising
    image = denoise(image, DENOISE_METHOD, DENOISE_STRENGTH)

    # Maximum intensity projections
    image_xy = image.max(axis=0).astype(np.float32)
    image_xz = image.max(axis=1).astype(np.float32)

    # Build slices
    slices = np.stack([image_xy, image_xz], axis=-1).astype(np.float32)

    # Convert to three channels
    slices = np.repeat(np.expand_dims(slices, axis=-1), 3, -1)

    # Normalize ImageNet
    slices -= [0.485, 0.456, 0.406]
    slices /= [0.229, 0.224, 0.225]

    # Add empty dimension to fit network
    slices = np.expand_dims(slices, 0)

    return slices


def predict(model, image):
    preprocessed_image = preprocess(image)

    prediction = model.predict(preprocessed_image).squeeze()
    prediction[:, 1:] *= image.shape

    return prediction


def main():
    parser = argparse.ArgumentParser(description='Prediction for the VerSe 2019 task')
    parser.add_argument('model', help='The fully trained model file')
    parser.add_argument('scan', help='The scan images we want to predict', nargs='+')
    parser.add_argument('-t', '--threshold', help='Confidence threshold', type=float, default=0.5)
    parser.add_argument('-p', '--plot', help='Whether to also plot the prediction on the image', action='store_true')

    args = parser.parse_args()

    if not os.path.exists(args.model):
        print(f'[!] The model {args.model} does not exist!')
        return

    print('[*] Loading model...', end=' ', flush=True)

    start = time.time()
    model = load_model(args.model, compile=False)
    end = time.time()

    print(f'done in {end - start:.02f}s')

    for scan_file in args.scan:
        if not os.path.exists(scan_file):
            print(f'[!] File {scan_file} does not exist!')
            continue
        elif not scan_file.endswith('.nii.gz'):
            print(f'[!] File {scan_file} should be in .nii.gz format!')
            continue

        print(f'[*] Predicting {scan_file}')

        image, spacing = load_image(scan_file)

        prediction = predict(model, image)

        if args.plot:
            plot_prediction(image, prediction, scan_file.replace('.nii.gz', '.png'), threshold=args.threshold)

        prediction[:, 1:] *= spacing
        prediction_to_json(prediction, scan_file.replace('.nii.gz', '.json'), threshold=args.threshold)


if __name__ == '__main__':
    main()
