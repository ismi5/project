import tensorflow as tf

from .core import Augmentation

class Cast(Augmentation):
    def __init__(self, *args, applies_to):
        super().__init__(1, applies_to)
        self.dtypes = args

    def apply_x(self, x_batch, y_batch):
        return tf.cast(x_batch, self.dtypes[0]), y_batch

    def apply_y(self, x_batch, y_batch):
        return x_batch, tf.cast(y_batch, self.dtypes[0])

    def apply_xy(self, x_batch, y_batch):
        return tf.cast(x_batch, self.dtypes[0]), tf.cast(y_batch, self.dtypes[1])

class CastX(Cast):
    def __init__(self, dtype):
        super().__init__(dtype, applies_to='x')

class CastY(Cast):
    def __init__(self, dtype):
        super().__init__(dtype, applies_to='y')

class CastXY(Cast):
    def __init__(self, x_dtype, y_dtype):
        super().__init__(x_dtype, y_dtype, applies_to='xy')

class Reshape(Augmentation):
    def __init__(self, *args, applies_to):
        super().__init__(1, applies_to)
        self.shapes = args

    def apply_x(self, x_batch, y_batch):
        return tf.reshape(x_batch, self.shapes[0]), y_batch

    def apply_y(self, x_batch, y_batch):
        return x_batch, tf.reshape(y_batch, self.shapes[0])

    def apply_xy(self, x_batch, y_batch):
        return tf.reshape(x_batch, self.y_shape[0]), tf.reshape(y_batch, self.y_shape[1])

class ReshapeX(Reshape):
    def __init__(self, shape):
        super().__init__(shape, applies_to='x')

class ReshapeY(Reshape):
    def __init__(self, shape):
        super().__init__(shape, applies_to='y')

class ReshapeXY(Reshape):
    def __init__(self, x_shape, y_shape):
        super().__init__(x_shape, y_shape, applies_to='xy')

class Mixup(Augmentation):
    def __init__(self, alpha=0.2, p=1):
        super().__init__(p, 'xy')

        self.alpha = tf.convert_to_tensor(alpha)

    def apply_xy(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]

        a, b = tf.random.gamma((batch_size,), self.alpha), tf.random.gamma((batch_size,), self.alpha)
        lam_y = tf.expand_dims(tf.abs(a / (a + b) - 0.5) + 0.5, axis=1)
        perms = tf.random.shuffle(tf.range(batch_size))

        lam_x = tf.expand_dims(lam_y, axes=(2, 3))
        return lam_x * x_batch + (1 - lam_x) * tf.gather(x_batch, perms, axis=0), lam_y * y_batch + (1 - lam_y) * tf.gather(y_batch, perms, axis=0)