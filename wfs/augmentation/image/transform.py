import tensorflow as tf
from numpy import pi

from ..core import Augmentation, Sequence
from . import affine

class HorizontalFlip(Augmentation):
    def __init__(self, p=0.5, applies_to='x'):
        super().__init__(p, applies_to)

    def apply_x(self, x_batch, y_batch):
        return tf.reverse(x_batch, [2]), y_batch

    def apply_y(self, x_batch, y_batch):
        return x_batch, tf.reverse(y_batch, [2])

    def apply_xy(self, x_batch, y_batch):
        return tf.reverse(x_batch, [2]), tf.reverse(y_batch, [2])

class VerticalFlip(Augmentation):
    def __init__(self, p=0.5, applies_to='x'):
        super().__init__(p, applies_to)

    def apply_x(self, x_batch, y_batch):
        return tf.reverse(x_batch, [1]), y_batch

    def apply_y(self, x_batch, y_batch):
        return x_batch, tf.reverse(y_batch, [1])

    def apply_xy(self, x_batch, y_batch):
        return tf.reverse(x_batch, [1]), tf.reverse(y_batch, [1])

# MOST OF THIS IS NOT TESTED AND MIGHT NOT BE OPTIMAL EITHER BUT THE IDEAS ARE THERE

class Shift(Augmentation):
    def __init__(self, max_shift_ratio, border_mode='zero', fill=None, p=1, applies_to='x'):
        super().__init__(p, applies_to)

        self.max_shift_ratio = tf.cast(max_shift_ratio, tf.float32)
        self.border_func, self.fill = affine.border_mode_string_to_func(border_mode, fill)

    def apply_x(self, x_batch, y_batch):
        shape = tf.shape(x_batch)
        batch_size = shape[0]

        y_image_size = tf.cast(shape[1], dtype=tf.float32)
        x_image_size = tf.cast(shape[2], dtype=tf.float32)

        x_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * x_image_size
        y_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * y_image_size

        x_batch = affine.shift_rotate_shear_zoom(x_batch, x_shifts=x_shifts, y_shifts=y_shifts, border_func=self.border_func, fill=self.fill)

        return x_batch, y_batch

    def apply_y(self, x_batch, y_batch):
        shape = tf.shape(y_batch)
        batch_size = shape[0]

        y_image_size = tf.cast(shape[1], dtype=tf.float32)
        x_image_size = tf.cast(shape[2], dtype=tf.float32)

        x_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * x_image_size
        y_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * y_image_size

        y_batch = affine.shift_rotate_shear_zoom(y_batch, x_shifts=x_shifts, y_shifts=y_shifts, border_func=self.border_func, fill=self.fill)

        return x_batch, y_batch

class ShiftZoom(Augmentation):
    def __init__(self, max_shift_ratio, zoom, zoom_upper=None, keep_aspect=True, border_mode='zero', fill=None, p=1, applies_to='x'):
        super().__init__(p, applies_to)

        self.max_shift_ratio = tf.cast(max_shift_ratio, tf.float32)
        if zoom_upper is None:
            zoom_lower = 1 - zoom
            zoom_upper = 1 + zoom
        else:
            zoom_lower = zoom
        self.zoom_lower = tf.cast(zoom_lower, tf.float32)
        self.zoom_upper = tf.cast(zoom_upper, tf.float32)

        self.keep_aspect = keep_aspect
        self.border_func, self.fill = affine.border_mode_string_to_func(border_mode, fill)

    def apply_x(self, x_batch, y_batch):
        shape = tf.shape(x_batch)
        batch_size = shape[0]
        
        y_image_size = tf.cast(shape[1], dtype=tf.float32)
        x_image_size = tf.cast(shape[2], dtype=tf.float32)

        x_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * x_image_size
        y_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * y_image_size

        if self.keep_aspect:
            zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)

            x_batch = affine.shift_rotate_shear_zoom(x_batch, x_shifts=x_shifts, y_shifts=y_shifts, x_zooms=zooms, y_zooms=zooms, border_func=self.border_func, fill=self.fill)

            return x_batch, y_batch

        x_zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)
        y_zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)

        x_batch = affine.shift_rotate_shear_zoom(x_batch, x_shifts=x_shifts, y_shifts=y_shifts, x_zooms=x_zooms, y_zooms=y_zooms, border_func=self.border_func, fill=self.fill)

        return x_batch, y_batch

    def apply_y(self, x_batch, y_batch):
        shape = tf.shape(y_batch)
        batch_size = shape[0]
        
        y_image_size = tf.cast(shape[1], dtype=tf.float32)
        x_image_size = tf.cast(shape[2], dtype=tf.float32)

        x_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * x_image_size
        y_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * y_image_size

        if self.keep_aspect:
            zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)

            y_batch = affine.shift_rotate_shear_zoom(y_batch, x_shifts=x_shifts, y_shifts=y_shifts, x_zooms=zooms, y_zooms=zooms, border_func=self.border_func, fill=self.fill)

            return x_batch, y_batch

        x_zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)
        y_zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)

        y_batch = affine.shift_rotate_shear_zoom(y_batch, x_shifts=x_shifts, y_shifts=y_shifts, x_zooms=x_zooms, y_zooms=y_zooms, border_func=self.border_func, fill=self.fill)

        return x_batch, y_batch

class ShiftRotate(Augmentation):
    def __init__(self, max_shift_ratio, max_angle, border_mode='zero', fill=None, p=1, applies_to='x'):
        super().__init__(p, applies_to)

        self.max_shift_ratio = tf.cast(max_shift_ratio, tf.float32)
        self.max_angle = tf.cast(max_angle, tf.float32)

        self.border_func, self.fill = affine.border_mode_string_to_func(border_mode, fill)

    def apply_x(self, x_batch, y_batch):
        shape = tf.shape(x_batch)
        batch_size = shape[0]
        
        y_image_size = tf.cast(shape[1], dtype=tf.float32)
        x_image_size = tf.cast(shape[2], dtype=tf.float32)

        x_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * x_image_size
        y_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * y_image_size

        angles = tf.random.uniform((batch_size,), -self.max_angle, self.max_angle, dtype=tf.float32)
        angles *= pi / 180
        
        x_batch = affine.shift_rotate_shear_zoom(x_batch, x_shifts=x_shifts, y_shifts=y_shifts, angles=angles, border_func=self.border_func, fill=self.fill)

        return x_batch, y_batch

    def apply_y(self, x_batch, y_batch):
        shape = tf.shape(y_batch)
        batch_size = shape[0]
        
        y_image_size = tf.cast(shape[1], dtype=tf.float32)
        x_image_size = tf.cast(shape[2], dtype=tf.float32)

        x_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * x_image_size
        y_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * y_image_size

        angles = tf.random.uniform((batch_size,), -self.max_angle, self.max_angle, dtype=tf.float32)
        angles *= pi / 180
        
        y_batch = affine.shift_rotate_shear_zoom(y_batch, x_shifts=x_shifts, y_shifts=y_shifts, angles=angles, border_func=self.border_func, fill=self.fill)

        return x_batch, y_batch

class ShiftRotateZoom(Augmentation):
    def __init__(self, max_shift_ratio, max_angle, zoom, zoom_upper=None, keep_aspect=True, border_mode='zero', fill=None, p=1, applies_to='x'):
        super().__init__(p, applies_to)

        self.max_shift_ratio = tf.cast(max_shift_ratio, tf.float32)
        self.max_angle = tf.cast(max_angle, tf.float32)
        if zoom_upper is None:
            zoom_lower = 1 - zoom
            zoom_upper = 1 + zoom
        else:
            zoom_lower = zoom
        self.zoom_lower = tf.cast(zoom_lower, tf.float32)
        self.zoom_upper = tf.cast(zoom_upper, tf.float32)

        self.keep_aspect = keep_aspect
        self.border_func, self.fill = affine.border_mode_string_to_func(border_mode, fill)

    def apply_x(self, x_batch, y_batch):
        shape = tf.shape(x_batch)
        batch_size = shape[0]
        
        y_image_size = tf.cast(shape[1], dtype=tf.float32)
        x_image_size = tf.cast(shape[2], dtype=tf.float32)

        x_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * x_image_size
        y_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * y_image_size

        angles = tf.random.uniform((batch_size,), -self.max_angle, self.max_angle, dtype=tf.float32)
        angles *= pi / 180

        if self.keep_aspect:
            zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)

            x_batch = affine.shift_rotate_shear_zoom(x_batch, x_shifts=x_shifts, y_shifts=y_shifts, angles=angles, x_zooms=zooms, y_zooms=zooms, border_func=self.border_func, fill=self.fill)

            return x_batch, y_batch

        x_zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)
        y_zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)

        x_batch = affine.shift_rotate_shear_zoom(x_batch, x_shifts=x_shifts, y_shifts=y_shifts, angles=angles, x_zooms=x_zooms, y_zooms=y_zooms, border_func=self.border_func, fill=self.fill)

        return x_batch, y_batch

    def apply_y(self, x_batch, y_batch):
        shape = tf.shape(x_batch)
        batch_size = shape[0]
        
        y_image_size = tf.cast(shape[1], dtype=tf.float32)
        x_image_size = tf.cast(shape[2], dtype=tf.float32)

        x_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * x_image_size
        y_shifts = tf.random.uniform((batch_size,), -self.max_shift_ratio, self.max_shift_ratio, dtype=tf.float32) * y_image_size

        angles = tf.random.uniform((batch_size,), -self.max_angle, self.max_angle, dtype=tf.float32)
        angles *= pi / 180

        if self.keep_aspect:
            zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)

            y_batch = affine.shift_rotate_shear_zoom(y_batch, x_shifts=x_shifts, y_shifts=y_shifts, angles=angles, x_zooms=zooms, y_zooms=zooms, border_func=self.border_func, fill=self.fill)

            return x_batch, y_batch

        x_zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)
        y_zooms = tf.random.uniform((batch_size,), self.zoom_lower, self.zoom_upper, dtype=tf.float32)

        y_batch = affine.shift_rotate_shear_zoom(y_batch, x_shifts=x_shifts, y_shifts=y_shifts, angles=angles, x_zooms=x_zooms, y_zooms=y_zooms, border_func=self.border_func, fill=self.fill)

        return x_batch, y_batch
