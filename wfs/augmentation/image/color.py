import tensorflow as tf

from ..core import Augmentation

class RGB_Brightness(Augmentation):    
    def __init__(self, image_max, ratio, p=1):
        super().__init__(p, 'x')
        self.image_max = image_max
        self.max_delta = ratio * image_max

    def apply_x(self, x_batch, y_batch):
        deltas = tf.random.uniform((tf.shape(x_batch)[0], 1, 1, 1), -self.max_delta, self.max_delta, dtype=tf.float32)

        return tf.clip_by_value(x_batch + deltas, 0, self.image_max), y_batch

def RGB_FLOAT_Brightness(ratio, p=1):
    return RGB_Brightness(1, ratio, p)

def RGB_UINT_Brightness(ratio, p=1):
    return RGB_Brightness(255, ratio, p)

class RGB_Contrast(Augmentation):
    def __init__(self, image_max, mixed, upper=None, p=1):
        super().__init__(p, 'x')
        self.image_max = image_max

        if upper is None:
            lower = 1 - mixed
            upper = 1 + mixed
        else:
            lower = mixed

        self.lower = lower
        self.upper = upper

    def apply_x(self, x_batch, y_batch):
        factors = tf.random.uniform((tf.shape(x_batch)[0], 1, 1, 1), self.lower, self.upper, dtype=tf.float32)
        means = tf.math.reduce_mean(x_batch, axis=(1, 2), keepdims=True)

        return tf.clip_by_value((x_batch - means) * factors + means, 0, self.image_max), y_batch

def RGB_FLOAT_Contrast(mixed, upper=None, p=1):
    return RGB_Contrast(1, mixed, upper, p)

def RGB_UINT_Contrast(mixed, upper=None, p=1):
    return RGB_Contrast(255, mixed, upper, p)

class RGB_FLOAT_to_HSV(Augmentation):
    def __init__(self, p=1):
        super().__init__(p, 'x')

    def apply_x(self, x_batch, y_batch):
        return tf.image.rgb_to_hsv(x_batch), y_batch

class HSV_to_RGB_FLOAT(Augmentation):
    def __init__(self, p=1):
        super().__init__(p, 'x')

    def apply_x(self, x_batch, y_batch):
        return tf.image.hsv_to_rgb(x_batch), y_batch

class HSV_Hue(Augmentation):
    def __init__(self, max_delta, p=1):
        super().__init__(p, 'x')
        self.max_delta = max_delta

    def apply_x(self, x_batch, y_batch):
        x_shape = tf.shape(x_batch)
        target_shape = tf.concat([x_shape[:1], tf.ones(tf.size(x_shape) - 1, dtype=tf.int32)], axis=0)

        deltas = tf.random.uniform(target_shape, -self.max_delta, self.max_delta, dtype=tf.float32)
        zeros = tf.zeros(target_shape, dtype=tf.float32)

        deltas = tf.concat([deltas, zeros, zeros], axis=-1)

        x_batch = tf.clip_by_value(tf.math.add(x_batch, deltas), 0, 1)

        return x_batch, y_batch

class HSV_Saturation(Augmentation):
    def __init__(self, max_delta, p=1):
        super().__init__(p, 'x')
        self.max_delta = max_delta

    def apply_x(self, x_batch, y_batch):
        x_shape = tf.shape(x_batch)
        target_shape = tf.concat([x_shape[:1], tf.ones(tf.size(x_shape) - 1, dtype=tf.int32)], axis=0)

        deltas = tf.random.uniform(target_shape, -self.max_delta, self.max_delta, dtype=tf.float32)
        zeros = tf.zeros(target_shape, dtype=tf.float32)

        deltas = tf.concat([zeros, deltas, zeros], axis=-1)

        x_batch = tf.clip_by_value(tf.math.add(x_batch, deltas), 0, 1)

        return x_batch, y_batch

class HSV_Value(Augmentation):
    def __init__(self, max_delta, p=1):
        super().__init__(p, 'x')
        self.max_delta = max_delta

    def apply_x(self, x_batch, y_batch):
        x_shape = tf.shape(x_batch)
        target_shape = tf.concat([x_shape[:1], tf.ones(tf.size(x_shape) - 1, dtype=tf.int32)], axis=0)

        deltas = tf.random.uniform(target_shape, -self.max_delta, self.max_delta, dtype=tf.float32)
        zeros = tf.zeros(target_shape, dtype=tf.float32)

        deltas = tf.concat([zeros, zeros, deltas], axis=-1)

        x_batch = tf.clip_by_value(tf.math.add(x_batch, deltas), 0, 1)

        return x_batch, y_batch

class HSV_Hue_Saturation(Augmentation):
    def __init__(self, max_delta, p=1):
        super().__init__(p, 'x')
        self.max_delta = max_delta

    def apply_x(self, x_batch, y_batch):
        x_shape = tf.shape(x_batch)
        target_shape = tf.concat([x_shape[:1], tf.ones(tf.size(x_shape) - 1, dtype=tf.int32)], axis=0)

        deltas_a = tf.random.uniform(target_shape, -self.max_delta, self.max_delta, dtype=tf.float32)
        deltas_b = tf.random.uniform(target_shape, -self.max_delta, self.max_delta, dtype=tf.float32)
        zeros = tf.zeros(target_shape, dtype=tf.float32)

        deltas = tf.concat([deltas_a, deltas_b, zeros], axis=-1)

        x_batch = tf.clip_by_value(tf.math.add(x_batch, deltas), 0, 1)

        return x_batch, y_batch

class HSV_Saturation_Value(Augmentation):
    def __init__(self, max_delta, p=1):
        super().__init__(p, 'x')
        self.max_delta = max_delta

    def apply_x(self, x_batch, y_batch):
        x_shape = tf.shape(x_batch)
        target_shape = tf.concat([x_shape[:1], tf.ones(tf.size(x_shape) - 1, dtype=tf.int32)], axis=0)

        deltas_a = tf.random.uniform(target_shape, -self.max_delta, self.max_delta, dtype=tf.float32)
        deltas_b = tf.random.uniform(target_shape, -self.max_delta, self.max_delta, dtype=tf.float32)
        zeros = tf.zeros(target_shape, dtype=tf.float32)

        deltas = tf.concat([zeros, deltas_a, deltas_b], axis=-1)

        x_batch = tf.clip_by_value(tf.math.add(x_batch, deltas), 0, 1)

        return x_batch, y_batch

class HSV_Hue_Saturation_Value(Augmentation):
    def __init__(self, max_delta, p=1):
        super().__init__(p, 'x')
        self.max_delta = max_delta

    def apply_x(self, x_batch, y_batch):
        x_shape = tf.shape(x_batch)
        target_shape = tf.concat([x_shape[:1], tf.ones(tf.size(x_shape) - 1, dtype=tf.int32)], axis=0)

        deltas_a = tf.random.uniform(target_shape, -self.max_delta, self.max_delta, dtype=tf.float32)
        deltas_b = tf.random.uniform(target_shape, -self.max_delta, self.max_delta, dtype=tf.float32)
        deltas_c = tf.random.uniform(target_shape, -self.max_delta, self.max_delta, dtype=tf.float32)

        deltas = tf.concat([deltas_a, deltas_b, deltas_c], axis=-1)

        x_batch = tf.clip_by_value(tf.math.add(x_batch, deltas), 0, 1)

        return x_batch, y_batch