class HorizontalShift(Augmentation):
    def __init__(self, max_shift_ratio, border_mode='reflect', p=1):
        super().__init__(p, 'x')

        self.max_shift_ratio = max_shift_ratio
        self.border_func = border_mode_string_to_func(border_mode)


    def apply_x(self, x_batch, y_batch):
        shape = tf.shape(x_batch)
        batch_size = shape[0]
        image_size = shape[1]

        max_delta = tf.cast(image_size, tf.float32) * self.max_shift_ratio
        max_delta = tf.cast(tf.math.round(max_delta), tf.int32)

        shifts = tf.random.uniform((batch_size, 1), -max_delta, max_delta, dtype=tf.int32)
        xs = tf.reshape(tf.tile(tf.range(image_size, dtype=tf.int32), [batch_size]), (batch_size, image_size))
        indices = self.border_func(xs - shifts, image_size, fill=-1)

        matrix = tf.one_hot(indices, depth=image_size, dtype=tf.int32)

        return tf.einsum('bijc,bkj->bikc', x_batch, matrix), y_batch

class VerticalShift(Augmentation):
    def __init__(self, max_shift_ratio, border_mode='reflect', p=1):
        super().__init__(p, 'x')

        self.max_shift_ratio = max_shift_ratio
        self.border_func = border_mode_string_to_func(border_mode)

    def apply_x(self, x_batch, y_batch):
        shape = tf.shape(x_batch)
        batch_size = shape[0]
        image_size = shape[1]
        
        max_delta = tf.cast(image_size, tf.float32) * self.max_shift_ratio
        max_delta = tf.cast(tf.math.round(max_delta), tf.int32)

        shifts = tf.random.uniform((batch_size, 1), -max_delta, max_delta, dtype=tf.int32)
        xs = tf.reshape(tf.tile(tf.range(image_size, dtype=tf.int32), [batch_size]), (batch_size, image_size))
        indices = self.border_func(xs - shifts, image_size, fill=-1)

        matrix = tf.one_hot(indices, depth=image_size, dtype=tf.int32)

        return tf.einsum('bjic,bkj->bkic', x_batch, matrix), y_batch

def Shift(max_shift_ratio, border_mode='reflect'):
    return Sequence(
        HorizontalShift(max_shift_ratio, border_mode),
        VerticalShift(max_shift_ratio, border_mode)
    )