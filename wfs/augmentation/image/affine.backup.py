import tensorflow as tf
from numpy import pi

def border_mode_string_to_func(border_mode, fill=None):
    if border_mode == 'reflect':
        return border_reflect, None

    elif border_mode == 'repeat':
        return border_repeat, None

    elif border_mode == 'zero':
        return border_zero, None

    elif border_constant == 'constant':
        if fill == [0, 0, 0]:
            return border_zero, None
            
        return border_constant, tf.convert_to_tensor(fill)

    raise ValueError('Unknown border mode: %s.' % border_mode)

def border_reflect(indices, image_size):
    return -tf.abs(image_size - tf.abs(indices) - 1) + image_size - 1

def border_repeat(indices, image_size):
    return tf.clip_by_value(indices, 0, image_size - 1)

def border_constant(indices, image_size):
    cond = tf.math.logical_and(tf.math.less(indices, image_size), tf.math.greater_equal(indices, 0))
    cond = tf.math.reduce_all(cond, axis=2, keepdims=True)
    
    return tf.where(cond, indices, 0)

def border_zero(indices, image_size):
    cond = tf.math.logical_and(tf.math.less(indices, image_size), tf.math.greater_equal(indices, 0))
    cond = tf.math.reduce_all(cond, axis=2, keepdims=True)
    
    return tf.where(cond, indices, -image_size* image_size)

# fill must be a tensor of the form [r, g, b]
# note that fill is rather slow
def shift_rotate_shear_zoom(x_batch, x_shifts, y_shifts, angles, x_shears, y_shears, x_zooms, y_zooms, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)

    angles *= pi / 180
    x_shears = tf.expand_dims(-2 * tf.math.tan(angles / 2) + x_shears, axis=1) * y
    y_shears = tf.expand_dims(tf.math.sin(angles) + y_shears, axis=1) * x

    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices /= tf.expand_dims(tf.stack([x_zooms, y_zooms], axis=1), axis=1)
    indices -= tf.stack([x_shears, y_shears], axis=2)
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift_rotate_shear_zoom_keep_aspect(x_batch, x_shifts, y_shifts, angles, x_shears, y_shears, zooms, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)

    angles *= pi / 180
    x_shears = tf.expand_dims(-2 * tf.math.tan(angles / 2) + x_shears, axis=1) * y
    y_shears = tf.expand_dims(tf.math.sin(angles) + y_shears, axis=1) * x

    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices /= tf.expand_dims(zooms, axes=(1, 2))
    indices -= tf.stack([x_shears, y_shears], axis=2)
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift_shear_zoom(x_batch, x_shifts, y_shifts, x_shears, y_shears, x_zooms, y_zooms, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)

    x_shears = tf.expand_dims(x_shears, axis=1) * y
    y_shears = tf.expand_dims(y_shears, axis=1) * x
    
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices /= tf.expand_dims(tf.stack([x_zooms, y_zooms], axis=1), axis=1)
    indices -= tf.stack([x_shears, y_shears], axis=2)
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift_shear_zoom_keep_aspect(x_batch, x_shifts, y_shifts, x_shears, y_shears, zooms, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)

    x_shears = tf.expand_dims(x_shears, axis=1) * y
    y_shears = tf.expand_dims(y_shears, axis=1) * x
    
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices /= tf.expand_dims(zooms, axes=(1, 2))
    indices -= tf.stack([x_shears, y_shears], axis=2)
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift_rotate_zoom(x_batch, x_shifts, y_shifts, angles, x_zooms, y_zooms, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)

    angles *= pi / 180
    x_shears = tf.expand_dims(-2 * tf.math.tan(angles / 2), axis=1) * y
    y_shears = tf.expand_dims(tf.math.sin(angles), axis=1) * x
    
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices /= tf.expand_dims(tf.stack([x_zooms, y_zooms], axis=1), axis=1)
    indices -= tf.stack([x_shears, y_shears], axis=2)
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift_rotate_zoom_keep_aspect(x_batch, x_shifts, y_shifts, angles, zooms, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)

    angles *= pi / 180
    x_shears = tf.expand_dims(-2 * tf.math.tan(angles / 2), axis=1) * y
    y_shears = tf.expand_dims(tf.math.sin(angles), axis=1) * x
    
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices /= tf.expand_dims(zooms, axes=(1, 2))
    indices -= tf.stack([x_shears, y_shears], axis=2)
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift_rotate_shear(x_batch, x_shifts, y_shifts, angles, x_shears, y_shears, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)

    angles *= pi / 180
    x_shears = tf.expand_dims(-2 * tf.math.tan(angles / 2) + x_shears, axis=1) * y
    y_shears = tf.expand_dims(tf.math.sin(angles) + y_shears, axis=1) * x
    
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices -= tf.stack([x_shears, y_shears], axis=2)
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift_rotate(x_batch, x_shifts, y_shifts, angles, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)

    angles *= pi / 180
    x_shears = tf.expand_dims(-2 * tf.math.tan(angles / 2), axis=1) * y
    y_shears = tf.expand_dims(tf.math.sin(angles), axis=1) * x
    
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices -= tf.stack([x_shears, y_shears], axis=2)
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift_shear(x_batch, x_shifts, y_shifts, x_shears, y_shears, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)

    x_shears = tf.expand_dims(x_shears, axis=1) * y
    y_shears = tf.expand_dims(y_shears, axis=1) * x
    
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices -= tf.stack([x_shears, y_shears], axis=2)
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift_zoom(x_batch, x_shifts, y_shifts, x_zooms, y_zooms, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)
    
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices /= tf.expand_dims(tf.stack([x_zooms, y_zooms], axis=1), axis=1)
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift_zoom_keep_aspect(x_batch, x_shifts, y_shifts, zooms, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_half = image_size_float // 2
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32) - image_size_half
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)
    
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices /= tf.expand_dims(zooms, axes=(1, 2))
    indices += tf.expand_dims(image_size_half - tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch

def shift(x_batch, x_shifts, y_shifts, border_func=border_zero, fill=None):
    shape = tf.shape(x_batch)
    batch_size = shape[0]
    image_size = shape[1]
    image_size_float = tf.cast(image_size, tf.float32)
    image_size_square = image_size * image_size
    total_size = batch_size * image_size_square
    
    row = tf.range(image_size_float, dtype=tf.float32)
    x = tf.tile(row, [image_size])
    y = tf.repeat(row, image_size)
    
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)

    indices += tf.expand_dims(-tf.stack([x_shifts, y_shifts], axis=1), axis=1)

    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices = border_func(indices, image_size)
    indices = tf.reduce_sum(indices * [1, image_size], axis=2)
    indices = tf.reshape(indices + image_size_square * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))
    
    if fill is not None:
        x_batch = tf.reshape(x_batch, (batch_size, image_size_square, 3))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= x_batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_size_square - 1, 3), tf.int32)], axis=1)        
        x_batch += filler
        
    x_batch = tf.reshape(x_batch, (total_size, 3))
    x_batch = tf.gather(x_batch, indices, axis=0)
    x_batch = tf.reshape(x_batch, (batch_size, image_size, image_size, 3))
    
    return x_batch