import tensorflow as tf

def border_mode_string_to_func(border_mode, fill=None):
    if border_mode == 'reflect':
        return border_reflect, None

    elif border_mode == 'repeat':
        return border_repeat, None

    elif border_mode == 'zero':
        return border_zero, None

    elif border_mode == 'constant' or border_mode == 'fill':
        if fill is None:
             raise ValueError('Fill cannot not be None when using border mode %s.' % border_mode)

        return border_constant, tf.convert_to_tensor(fill, dtype=tf.float32)

    raise ValueError('Unknown border mode: %s.' % border_mode)

def reflect(indices, image_size):
    return -tf.abs(image_size - tf.abs(indices) - 1) + image_size - 1

def border_reflect(indices_x, indices_y, image_size_x, image_size_y):
    return reflect(indices_x, image_size_x), reflect(indices_y, image_size_y)

def repeat(indices, image_size):
    return tf.clip_by_value(indices, 0, image_size - 1)

def border_repeat(indices_x, indices_y, image_size_x, image_size_y):
    return repeat(indices_x, image_size_x), repeat(indices_y, image_size_y)

def border_constant(indices_x, indices_y, image_size_x, image_size_y):
    cond_x = tf.math.logical_and(tf.math.less(indices_x, image_size_x), tf.math.greater_equal(indices_x, 0))
    cond_y = tf.math.logical_and(tf.math.less(indices_y, image_size_y), tf.math.greater_equal(indices_y, 0))
    
    cond = tf.math.reduce_all(tf.concat([cond_x, cond_y], axis=2), axis=2, keepdims=True)
    
    return tf.where(cond, indices_x, 0), tf.where(cond, indices_y, 0)

def border_zero(indices_x, indices_y, image_size_x, image_size_y):
    cond_x = tf.math.logical_and(tf.math.less(indices_x, image_size_x), tf.math.greater_equal(indices_x, 0))
    cond_y = tf.math.logical_and(tf.math.less(indices_y, image_size_y), tf.math.greater_equal(indices_y, 0))
    
    cond = tf.math.reduce_all(tf.concat([cond_x, cond_y], axis=2), axis=2, keepdims=True)
    
    return tf.where(cond, indices_x, -image_size * image_size), tf.where(cond, indices_y, -image_size * image_size)

def guarantee_rank_one(tensor):
    if tf.rank(tensor) == 0:
        return tf.expand_dims(tensor, 0)

    return tensor

# fill must be a tensor of the form [scalar per channel in image]
# angles should be passed in radians
# note that fill is rather slow
def shift_rotate_shear_zoom(batch, x_shifts=None, y_shifts=None, angles=None, x_shears=None, y_shears=None, x_zooms=None, y_zooms=None, border_func=border_zero, fill=None):
    shape = tf.shape(batch)
    
    # Calculate shapes
    batch_size = shape[0]
    image_size_y = shape[1]
    image_size_x = shape[2]
    channel_size = shape[3]

    image_size_float_y = tf.cast(image_size_y, tf.float32)
    image_size_float_x = tf.cast(image_size_x, tf.float32)
    image_size_half_y = image_size_float_y // 2
    image_size_half_x = image_size_float_x // 2
    image_size_halfs = tf.convert_to_tensor([image_size_half_x, image_size_half_y])
    image_total_size = image_size_y * image_size_x
    total_size = batch_size * image_total_size

    # Interpret shifts
    if x_shifts is not None and y_shifts is None:
        y_shifts = tf.zeros(1, dtype=tf.float32)
        
    elif x_shifts is None and y_shifts is not None:
        x_shifts = tf.zeros(1, dtype=tf.float32)

    # Interpret zooms
    if x_zooms is not None and y_zooms is None:
        y_zooms = tf.ones(1, dtype=tf.float32)
        
    elif x_zooms is None and y_zooms is not None:
        x_zooms = tf.ones(1, dtype=tf.float32)
    
    # Build indexers
    row_x = tf.range(image_size_float_x, dtype=tf.float32) 
    row_y = tf.range(image_size_float_y, dtype=tf.float32)
    
    if angles is not None or x_shears is not None or y_shears is not None or x_zooms is not None:
        row_x -= image_size_half_x
        row_y -= image_size_half_y
    
    x = tf.tile(row_x, [image_size_y])
    y = tf.repeat(row_y, image_size_x)

    # Build indices
    indices = tf.stack([x, y], axis=1)
    indices = tf.repeat([indices], batch_size, axis=0)
    
    # Calculate shears from given shears and potentially rotation angles
    if angles is not None:
        angles = guarantee_rank_one(angles)
        
        if x_shears is not None and y_shears is None:
            y_shears = tf.zeros(1, dtype=tf.float32)
        
        elif x_shears is None and y_shears is not None:
            x_shears = tf.zeros(1, dtype=tf.float32)
        
        if x_shears is not None:
            x_shears = guarantee_rank_one(x_shears)
            y_shears = guarantee_rank_one(y_shears)
            
            x_shears = tf.expand_dims(-2 * tf.math.tan(-angles / 2) + x_shears, axis=1) * y
            y_shears = tf.expand_dims(tf.math.sin(-angles) + y_shears, axis=1) * x
            
        else:
            x_shears = tf.expand_dims(-2 * tf.math.tan(-angles / 2), axis=1) * y
            y_shears = tf.expand_dims(tf.math.sin(-angles), axis=1) * x
            
    else:
        if x_shears is not None and y_shears is None:
            y_shears = tf.zeros(1, dtype=tf.float32)
        
        elif x_shears is None and y_shears is not None:
            x_shears = tf.zeros(1, dtype=tf.float32)
        
        if x_shears is not None:
            x_shears = guarantee_rank_one(x_shears)
            y_shears = guarantee_rank_one(y_shears)

            x_shears = tf.expand_dims(x_shears, axis=1) * y
            y_shears = tf.expand_dims(y_shears, axis=1) * x
        
    # Apply zooms
    if x_zooms is not None:
        x_zooms = guarantee_rank_one(x_zooms)
        y_zooms = guarantee_rank_one(y_zooms)
        indices /= tf.expand_dims(tf.stack([x_zooms, y_zooms], axis=1), axis=1)

    # Apply shears
    if x_shears is not None:
        x_shears = guarantee_rank_one(x_shears)
        y_shears = guarantee_rank_one(y_shears)
        indices -= tf.stack([x_shears, y_shears], axis=2)

    # Apply shifts
    if x_shifts is not None:
        x_shifts = guarantee_rank_one(x_shifts)
        y_shifts = guarantee_rank_one(y_shifts)
        if x_shears is not None or x_zooms is not None:
            indices += image_size_halfs - tf.expand_dims(tf.stack([x_shifts, y_shifts], axis=1), axis=1)
        else:
            indices -= tf.stack([x_shifts, y_shifts], axis=1)
            
    elif x_shears is not None or x_zooms is not None:
        indices += image_size_halfs

    # Apply border function
    indices = tf.cast(tf.math.round(indices), tf.int32)
    indices_x, indices_y = indices[:, :, :1], indices[:, :, 1:]
    indices_x, indices_y = border_func(indices_x, indices_y, image_size_x, image_size_y)
    indices = tf.concat([indices_x, indices_y], axis=2)

    # Calculate flat indices
    indices = tf.reduce_sum(indices * [1, image_size_x], axis=2)
    indices = tf.reshape(indices + image_total_size * tf.expand_dims(tf.range(batch_size), axis=1), (total_size,))

    # Fill with given color
    if fill is not None:
        batch = tf.reshape(batch, (batch_size, image_total_size, channel_size))
        filler = tf.tile([fill], [batch_size, 1])
        filler -= batch[:, 0]
        filler = tf.expand_dims(filler, axis=1)
        filler = tf.concat([filler, tf.zeros((batch_size, image_total_size - 1, channel_size), tf.float32)], axis=1)        
        batch += filler

    # Apply affines to the batch
    batch = tf.reshape(batch, (total_size, channel_size))
    batch = tf.gather(batch, indices, axis=0)
    batch = tf.reshape(batch, (batch_size, image_size_y, image_size_x, channel_size))
    
    return batch