import tensorflow as tf

from ..core import Augmentation

class INT_to_FLOAT(Augmentation):
    def __init__(self, applies_to='x'):
        super().__init__(1, applies_to)

    def apply_x(self, x_batch, y_batch):
        x_batch = tf.cast(x_batch, tf.float32)
        x_batch /= 255

        return x_batch, y_batch

    def apply_y(self, x_batch, y_batch):
        y_batch = tf.cast(y_batch, tf.float32)
        y_batch /= 255

        return x_batch, y_batch

    def apply_xy(self, x_batch, y_batch):
        x_batch = tf.cast(x_batch, tf.float32)
        x_batch /= 255

        y_batch = tf.cast(y_batch, tf.float32)
        y_batch /= 255

        return x_batch, y_batch

class FLOAT_to_INT(Augmentation):
    def __init__(self, dtype=tf.uint8, applies_to='x'):
        super().__init__(1, applies_to)

    def apply_x(self, x_batch, y_batch):
        x_batch *= 255
        x_batch = tf.math.round(x_batch)
        x_batch = tf.cast(x_batch, tf.uint8)

        return x_batch, y_batch

    def apply_x(self, x_batch, y_batch):
        y_batch *= 255
        y_batch = tf.math.round(y_batch)
        y_batch = tf.cast(y_batch, tf.uint8)

        return x_batch, y_batch

    def apply_xy(self, x_batch, y_batch):
        x_batch *= 255
        x_batch = tf.math.round(x_batch)
        x_batch = tf.cast(x_batch, tf.uint8)

        y_batch *= 255
        y_batch = tf.math.round(y_batch)
        y_batch = tf.cast(y_batch, tf.uint8)

        return x_batch, y_batch

class Normalize(Augmentation):
    def __init__(self, mean, std):
        super().__init__(1, 'x')
        self.mean = tf.convert_to_tensor(mean, tf.float32)
        self.std = tf.convert_to_tensor(std, tf.float32)

    def apply_x(self, x_batch, y_batch):
        x_batch -= self.mean
        x_batch /= self.std

        return x_batch, y_batch

class NormalizeImagenet(Normalize):
    def __init__(self):
        super().__init__([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

class ToTripleChannel(Augmentation):
    def __init__(self):
        super().__init__(1, 'x')

    def apply_x(self, x_batch, y_batch):
        x_batch = tf.repeat(tf.expand_dims(x_batch, axis=-1), 3, -1)

        return x_batch, y_batch