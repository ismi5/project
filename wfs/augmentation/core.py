from abc import ABC, abstractmethod
import tensorflow as tf

def auto_applies_to(augmentations):
    has_x = has_y = False

    for augmentation in augmentations:
        if augmentation.applies_to == 'x':
            if has_y:
                return 'xy'

            has_x = True

        if augmentation.applies_to == 'y':
            if has_x:
                return 'xy'

            has_y = True

        if augmentation.applies_to == 'xy':
            return 'xy'

    if has_x:
        return 'x'

    if has_y:
        return 'y'

# Either override apply or override apply_x, apply_y and apply_xy separately
class Augmentation(ABC):
    def __init__(self, p, applies_to):
        self.p = tf.convert_to_tensor(p, dtype=tf.float32)
        self.applies_to = applies_to

        if self.applies_to == 'x':
            self.apply = self.apply_x

        elif self.applies_to == 'y':
            self.apply = self.apply_y

        elif self.applies_to == 'xy':
            self.apply = self.apply_xy

        else:
            raise ValueError('%s is not a valid applies_to value.' % applies_to)

        if self.p == 1:
            self.make_guaranteed()

        else:
            self.make_chanceable()
            

    def make_guaranteed(self):
        self.apply_with_chance = self.apply

    def make_chanceable(self):
        if self.applies_to == 'x':
            self.apply_with_chance = self.apply_with_chance_collective_x

        elif self.applies_to == 'y':
            self.apply_with_chance = self.apply_with_chance_collective_y

        elif self.applies_to == 'xy':
            self.apply_with_chance = self.apply_with_chance_collective_xy

        else:
            raise ValueError('%s is not a valid applies_to value.' % applies_to)

    # @tf.function
    def __call__(self, x_batch, y_batch):
        return self.apply_with_chance(x_batch, y_batch)

    def apply_with_chance_collective_x(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]

        indices = tf.math.less(tf.random.uniform((batch_size, 1, 1, 1)), self.p)
        x_indices = tf.cast(indices, x_batch.dtype)

        x_batch_applied, _ = self.apply_x(x_batch, y_batch)

        return x_indices * x_batch_applied + (1 - x_indices) * x_batch, y_batch

    def apply_with_chance_collective_y(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]

        indices = tf.math.less(tf.random.uniform((batch_size, 1)), self.p)
        y_indices = tf.cast(indices, y_batch.dtype)

        _, y_batch_applied = self.apply_y(x_batch, y_batch)

        return x_batch, y_indices * y_batch_applied + (1 - y_indices) * y_batch

    def apply_with_chance_collective_xy(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]

        indices = tf.math.less(tf.random.uniform((batch_size, 1)), self.p)
        x_indices = tf.expand_dims(tf.cast(indices, x_batch.dtype), axes=(2, 3))
        y_indices = tf.cast(indices, y_batch.dtype)

        x_batch_applied, y_batch_applied = self.apply_xy(x_batch, y_batch)

        return x_indices * x_batch_applied + (1 - x_indices) * x_batch, y_indices * y_batch_applied + (1 - y_indices) * y_batch
    
    def apply_x(self, x_batch, y_batch):
        raise NotImplementedError('%s does not support apply_x.' % self)

    def apply_y(self, x_batch, y_batch):
        raise NotImplementedError('%s does not support apply_y.' % self)

    def apply_xy(self, x_batch, y_batch):
        raise NotImplementedError('%s does not support apply_xy.' % self)

class Sequence(Augmentation):
    def __init__(self, *args, p=1, applies_to='auto'):
        if isinstance(args[0], list):
            augmentations = args[0]
        else:
            augmentations = args

        applies_to = auto_applies_to(augmentations) if applies_to == 'auto' else applies_to

        self.augmentations = []
        for augmentation in augmentations:
            if isinstance(augmentation, Sequence) and augmentation.applies_to == applies_to:
                self.augmentations.extend(augmentation.augmentations)
            else:
                self.augmentations.append(augmentation)

        super().__init__(p, applies_to)

    def apply_x(self, x_batch, y_batch):
        for augmentation in self.augmentations:
            x_batch, _ = augmentation.apply_with_chance(x_batch, None)

        return x_batch, y_batch

    def apply_y(self, x_batch, y_batch):
        for augmentation in self.augmentations:
            _, y_batch = augmentation.apply_with_chance(None, y_batch)

        return x_batch, y_batch

    def apply_xy(self, x_batch, y_batch):
        for augmentation in self.augmentations:
            x_batch, y_batch = augmentation.apply_with_chance(x_batch, y_batch)

        return x_batch, y_batch

class Either(Augmentation):
    def __init__(self, augmentation_1, augmentation_2, p=1, applies_to='auto'):
        self.augmentation_1 = augmentation_1
        self.augmentation_2 = augmentation_2

        applies_to = auto_applies_to([augmentation_1, augmentation_2]) if applies_to == 'auto' else applies_to
        super().__init__(p, applies_to)

        self.chance_on_augmentation_1 = augmentation_1.p / (augmentation_1.p + augmentation_2.p)

        augmentation_1.make_guaranteed()
        augmentation_2.make_guaranteed()
        

    def apply_x(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]

        indices = tf.math.less(tf.random.uniform((batch_size, 1, 1, 1)), self.chance_on_augmentation_1)
        x_indices = tf.cast(indices, x_batch.dtype)

        x_batch_1, _ = self.augmentation_1.apply(x_batch, None)
        x_batch_2, _ = self.augmentation_2.apply(x_batch, None)

        return x_indices * x_batch_1 + (1 - x_indices) * x_batch_2, y_batch

    def apply_y(self, x_batch, y_batch):
        batch_size = tf.shape(y_batch)[0]

        indices = tf.math.less(tf.random.uniform((batch_size, 1)), self.chance_on_augmentation_1)
        y_indices = tf.cast(indices, y_batch.dtype)

        _, y_batch_1 = self.augmentation_1.apply(None, y_batch)
        _, y_batch_2 = self.augmentation_2.apply(None, y_batch)

        return x_batch, y_indices * y_batch_1 + (1 - y_indices) * y_batch_2

    def apply_xy(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]

        indices = tf.math.less(tf.random.uniform((batch_size, 1)), self.chance_on_augmentation_1)
        x_indices = tf.expand_dims(tf.cast(indices, x_batch.dtype), axes=(2, 3))
        y_indices = tf.cast(indices, y_batch.dtype)

        x_batch_1, y_batch_1 = self.augmentation_1.apply(x_batch, y_batch)
        x_batch_2, y_batch_2 = self.augmentation_2.apply(x_batch, y_batch)

        return x_indices * x_batch_1 + (1 - x_indices) * x_batch_2, y_indices * y_batch_1 + (1 - y_indices) * y_batch_2

class OneOf(Augmentation):
    def __init__(self, *args, p=1, applies_to='auto'):
        if isinstance(args[0], list):
            self.augmentations = args[0]
        else:
            self.augmentations = args

        applies_to = auto_applies_to(self.augmentations) if applies_to == 'auto' else applies_to
        
        super().__init__(p, applies_to)

        self.ps = tf.convert_to_tensor([augmentation.p for augmentation in self.augmentations], dtype=tf.float32)
        self.ps /= tf.math.reduce_sum(self.ps)
        self.ps = tf.math.cumsum(self.ps)

        for augmentation in self.augmentations:
            augmentation.make_guaranteed()

    def apply_x(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]
        randoms = tf.random.uniform((batch_size, 1, 1, 1))

        x_out = tf.zeros_like(x_batch)

        old_indices = tf.zeros_like(randoms, dtype=tf.bool)

        for p, augmentation in zip(self.ps, self.augmentations):
            new_indices = tf.math.less(randoms, p)
            new_indices = tf.math.logical_and(new_indices, tf.math.logical_xor(new_indices, old_indices))

            x_indices = tf.cast(new_indices, x_batch.dtype)

            x_batch_applied, _ = augmentation.apply(x_batch, None)

            x_out += x_indices * x_batch_applied

            old_indices = tf.math.logical_or(old_indices, new_indices)

        return x_out, y_batch

    def apply_y(self, x_batch, y_batch):
        batch_size = tf.shape(y_batch)[0]
        randoms = tf.random.uniform((batch_size, 1))

        y_out = tf.zeros_like(y_batch)

        old_indices = tf.zeros_like(randoms, dtype=tf.bool)

        for p, augmentation in zip(self.ps, self.augmentations):
            new_indices = tf.math.less(randoms, p)
            new_indices = tf.math.logical_and(new_indices, tf.math.logical_xor(new_indices, old_indices))

            y_indices = tf.cast(new_indices, y_batch.dtype)

            _, y_batch_applied = augmentation.apply(None, y_batch)

            y_out += y_indices * y_batch_applied

            old_indices = tf.math.logical_or(old_indices, new_indices)

        return x_batch, y_out

    def apply_xy(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]
        randoms = tf.random.uniform((batch_size, 1))

        x_out = tf.zeros_like(x_batch)
        y_out = tf.zeros_like(y_batch)

        old_indices = tf.zeros_like(randoms, dtype=tf.bool)

        for p, augmentation in zip(self.ps, self.augmentations):
            new_indices = tf.math.less(randoms, p)
            new_indices = tf.math.logical_and(new_indices, tf.math.logical_xor(new_indices, old_indices))

            x_indices =  tf.expand_dims(tf.cast(new_indices, x_batch.dtype), axes=(2, 3))
            y_indices = tf.cast(new_indices, y_batch.dtype)

            x_batch_applied, y_batch_applied = augmentation.apply(x_batch, y_batch)

            x_out += x_indices * x_batch_applied
            y_out += y_indices * y_batch_applied

            old_indices = tf.math.logical_or(old_indices, new_indices)

        return x_out, y_out

class NoOp(Augmentation):
    def __init__(self, applies_to):
        super().__init__(1, applies_to)

    def apply_x(self, x_batch, y_batch):
        return x_batch, y_batch

    def apply_y(self, x_batch, y_batch):
        return x_batch, y_batch

    def apply_xy(self, x_batch, y_batch):
        return x_batch, y_batch

class NoOpX(NoOp):
    def __init__(self):
        super().__init__('x')

class NoOpY(NoOp):
    def __init__(self):
        super().__init__('y')

class NoOpXY(NoOp):
    def __init__(self):
        super().__init__('xy')