import tensorflow as tf
import numpy as np
from time import perf_counter
from IPython.display import display
from tqdm.autonotebook import tqdm
import matplotlib.pyplot as plt

# Creates plots during training and automatically saves the model
# Plot period is in seconds
class Keras(tf.keras.callbacks.Callback):
    def __init__(self, trainer, plot_period=10, **kwargs):
        self.trainer = trainer
        self.plot_period = plot_period

        self.last_plot = 0
        self.train_steps = 0
        self.train_epochs = 0
        self.best_epoch = 0

        self.train_losses = []
        self.validation_losses = []
        self.metric_values = []
        self.best_validation_metric_value = None

        self.on_predict_batch_end = self.on_non_train_batch_end
        self.on_predict_begin = self.on_non_train_begin
        self.on_predict_end = self.on_non_train_end
        self.on_test_batch_end = self.on_non_train_batch_end
        self.on_test_begin = self.on_non_train_begin
        self.on_test_end = self.on_non_train_end

    def attach(self, data):
        self.data = data
        return self

    def set_model(self, model):
        self.model = model

    def on_train_begin(self, logs=None):
        self.last_plot = 0
        self.data_train = self.data

        self.plot_handle = display({'text/html': ''}, raw=True, display_id=True)

        self.epoch_bar = tqdm(total=self.params.get('epochs'), unit='epoch')

        if self.trainer.data.has_val and self.best_validation_metric_value is None:
            loss, metric_results = self.trainer.validate()

            self.validation_losses.append(loss)
            self.best_validation_metric_value = metric_results[0]
            self.metric_values.append(metric_results)


    def on_train_end(self, logs=None):
        self.epoch_bar.close()
        self.data = None
        self.data_train = None

    def on_epoch_begin(self, epoch, logs=None):
        num_samples = self.data_train.num_samples
        self.samples_bar = tqdm(total=num_samples, unit='sample', desc='Training', leave=False, smoothing=0.9)

    def on_epoch_end(self, epoch, logs=None):
        self.epoch_bar.update()
        self.samples_bar.close()

        self.train_epochs += 1

        if self.trainer.data.has_val:
            loss, metric_results = self.trainer.validate()
            self.validation_losses.append(loss)
            self.metric_values.append(metric_results)

            if metric_results[0] > self.best_validation_metric_value:
                self.best_validation_metric_value = metric_results[0]
        
        self.plot(offset=0)

    def on_train_batch_end(self, batch, logs=None):
        self.samples_bar.update(logs.get('size', 0))

        self.train_steps += 1
        self.train_losses.append(logs.get('loss'))
        
        now = perf_counter()
        if now > self.last_plot + self.plot_period:
            self.last_plot = now
            self.plot()

    def plot(self, offset=1):
        if not self.run_from_ipython():
            return
        xs = np.linspace(0, self.train_epochs + 1, self.data_train.num_batches * (self.train_epochs + 1))

        fig = plt.figure(figsize=(12, 4))
        if self.trainer.data.has_val:
            plt.subplot(1, 2, 1)

        # Plot training loss
        plt.plot(xs[:len(self.train_losses)], self.train_losses, label='Training loss', alpha=0.6)
        plt.title('Loss')
        plt.xlabel('Epoch')
        plt.xlim(0, self.train_epochs + offset)
        
        if self.trainer.data.has_val:
            # Plot validation loss
            plt.plot(np.arange(self.train_epochs + 1), self.validation_losses, label='Validation loss')
            plt.legend()

            # Plot validation metrics
            metric_values_np = np.array(self.metric_values, dtype=np.float32)

            mv_max = np.max(metric_values_np[:, 0])
            mv_max_epoch = np.argmax(metric_values_np[:, 0])

            text_x = -80 if mv_max_epoch > 0 else mv_max_epoch + 10
            text_y = 10 if mv_max < 0.7 else -20
            
            plt.subplot(1, 2, 2)
            for i, metric in enumerate(self.trainer.all_metrics):
                plt.plot(np.arange(self.train_epochs + 1), metric_values_np[:, i], '-o', label=metric.label, linewidth=1)

            plt.annotate('Best = %.4f' % mv_max, xy=(mv_max_epoch, mv_max), xytext=(text_x, text_y), textcoords='offset pixels', arrowprops=dict(arrowstyle='fancy'))
            plt.title('Validation Score')
            plt.xlabel('Epoch')
            plt.xlim(0, self.train_epochs + offset)
            plt.ylim(0, 1)
            plt.legend()

        self.plot_handle.update(fig)
        plt.close()

    def on_non_train_begin(self, logs=None):
        num_samples = self.data.num_samples
        self.samples_bar = tqdm(total=num_samples, unit='sample', desc='Predicting', leave=False, smoothing=0.9)

    def on_non_train_end(self, logs=None):
        self.samples_bar.close()
        self.data = None

    def on_non_train_batch_end(self, batch, logs=None):
        self.samples_bar.update(logs.get('outputs')[0].shape[0])

    def run_from_ipython(selfs):
        try:
            __IPYTHON__
            return True
        except NameError:
            return False