from abc import ABC, abstractmethod

class DataType(ABC):
    pass

class TRAIN(DataType):
    pass

class VAL(DataType):
    pass

class TEST(DataType):
    pass