from sklearn.model_selection import train_test_split
from category_encoders import OneHotEncoder

import wfs
import wfs.data_type
from. data import Data
from .tfdata import TFData
from .container import Container

class Auto():
    def __init__(self, *args, data_class='auto', data_target='auto', from_train_split='default', from_non_train_split=0.5, 
        y_encode_decode='auto', random_state=2020, **kwargs):

        self.data_class = data_class
        self.from_train_split = from_train_split
        self.from_non_train_split = from_non_train_split
        self.random_state = random_state
        self.kwargs = kwargs

        if len(args) > 3:
            raise ValueError('You cannot pass more than three data sets.')

        if data_target == 'auto':
            if len(args) == 1:
                if from_train_split is not None and not isinstance(args[0], Data):
                    data_target = 'train:val_from_train'
                else:
                    data_target = 'train'

            elif len(args) == 2:
                data_target = 'train:val'

            elif len(args) == 3:
                data_target = 'train:val:test'

        
        train, val, test = None, None, None

        if y_encode_decode is None:
            self.multi_label = None
            self.y_encode, self.y_decode = wfs.identity, wfs.identity

        if y_encode_decode == 'auto':
            self._build_encode_decode(args[0])

        if data_target == 'train':
            train = self._build_data(args[0], wfs.data_type.TRAIN)

        elif data_target == 'val':
            val = self._build_data(args[0], wfs.data_type.VAL)
            
        elif data_target == 'test':
            test = self._build_data(args[0], wfs.data_type.TEST)

        elif data_target == 'train:val':
            train = self._build_data(args[0], wfs.data_type.TRAIN)
            val = self._build_data(args[1], wfs.data_type.VAL)

        elif data_target == 'train:test':
            train = self._build_data(args[0], wfs.data_type.TRAIN)
            test = self._build_data(args[1], wfs.data_type.TEST)

        elif data_target == 'val:test':
            val = self._build_data(args[0], wfs.data_type.VAL)
            test = self._build_data(args[1], wfs.data_type.TEST)

        elif data_target == 'train:val:test':
            train = self._build_data(args[0], wfs.data_type.TRAIN)
            val = self._build_data(args[0], wfs.data_type.VAL)
            test = self._build_data(args[1], wfs.data_type.TEST)

        elif data_target == 'train:val_from_train':
            train, val = self._build_and_split_data(args[0], wfs.data_type.TRAIN, wfs.data_type.VAL)

        elif data_target == 'train:test_from_train':
            train, test = self._build_and_split_data(args[0], wfs.data_type.TRAIN, wfs.data_type.TEST)

        elif data_target == 'train:val_from_train:test':
            train, val = self._build_and_split_data(args[0], wfs.data_type.TRAIN, wfs.data_type.VAL)
            test = self._build_data(args[1], DataType.TEST)

        elif data_target == 'train:val:test_from_train':
            train, test = self._build_and_split_data(args[0], wfs.data_type.TRAIN, wfs.data_type.TEST)
            val = self._build_data(args[1], wfs.data_type.VAL)

        elif data_target == 'train:val:test_from_val':
            train = self._build_data(args[0], wfs.data_type.TRAIN)
            val, test = self._build_and_split_data(args[1], wfs.data_type.VAL, wfs.data_type.TEST, from_train=False)

        elif data_target == 'train:val_from_test:test':
            train = self._build_data(args[0], wfs.data_type.TRAIN)
            test, val = self._build_and_split_data(args[1], wfs.data_type.TEST, wfs.data_type.VAL, from_train=False)

        elif data_target == 'train:val_from_train:test_from_train':
            train, val, test = self._build_and_double_split_data(args[0])
            
        else:
            raise ValueError('Unknown data target: %s.' % data_target)

        self.out = Container(train=train, val=val, test=test)

    def _build_data(self, data, data_type):
        if self.working_with_wfs_data:
            if isinstance(data, Data):
                return data

            raise ValueError('If a wfs.data.Data is passed, all arguments must be wfs.data.Data.')

        if isinstance(data, tuple) and len(data) == 2: # (x, y) tuple
            x, y = data
            y = self.y_encode(y)

            data_class = TFData.from_numpy if self.data_class == 'auto' else self.data_class
            return data_class(x, y, data_type=data_type, **self.kwargs)

        if isinstance(data, (list, np.ndarray)):
            data_class = TFData.from_numpy if self.data_class == 'auto' else self.data_class
            return data_class(np.array(data), None, data_type=data_type, **self.kwargs)

        raise ValueError('Unknown data type.')

    def _build_and_split_data(self, data, data_type_1, data_type_2, from_train=True):
        split = self.from_train_split if from_train else self.from_non_train_split

        if self.working_with_wfs_data:
            if isinstance(data, Data):
                if not data.splittable:
                    raise ValueError('This data type does not support data splitting.')

                # todo split data set

            raise ValueError('If a wfs.data.Data is passed, all arguments must be wfs.data.Data.')

        if isinstance(data, (list, np.ndarray)):
            raise ValueError('This data type does not support data splitting.')

        if isinstance(data, tuple) and len(data) == 2: # (x, y) tuple
            if split == 'default':
                split = 0.1

            x, y = data
            
            y = self.y_encode(y)

            if self.multi_label:
                split = train_test_split(x, *y, test_size=split, random_state=self.random_state, stratify=y[0])
                x_1, x_2, y_1, y_2 = split[0], split[1], tuple(split[2::2]), tuple(split[3::2])
                
            else:
                x_1, x_2, y_1, y_2 = train_test_split(x, y, test_size=split, random_state=self.random_state, stratify=y)

            data_class = Sequence if self.data_class == 'auto' else self.data_class
            data_1 = data_class(x_1, y_1, data_type=data_type_1, **self.kwargs)
            data_2 = data_class(x_2, y_2, data_type=data_type_2, **self.kwargs)

            return data_1, data_2

        raise ValueError('Unknown data type.')

    def _build_and_double_split_data(self, data):
        split = self.from_train_split

        if self.working_with_wfs_data:
            if isinstance(data, Data):
                if not data.splittable:
                    raise ValueError('This data type does not support data splitting.')

                # todo split data set

            raise ValueError('If a wfs.data.Data is passed, all arguments must be wfs.data.Data.')

        if isinstance(data, (list, np.ndarray)):
            raise ValueError('This data type does not support data splitting.')

        if isinstance(data, tuple) and len(data) == 2: # (x, y) tuple
            if split == 'default':
                split = 0.2

            x, y = data
            y = self.y_encode(y)

            if self.multi_label:
                split = train_test_split(x, *y, test_size=split, random_state=self.random_state, stratify=y[0])
                x_train, x_temp, y_train, y_temp = split[0], split[1], tuple(split[2::2]), tuple(split[3::2])

                split = train_test_split(x_temp, *y_temp, test_size=self.from_non_train_split, random_state=self.random_state, stratify=y_temp[0])
                x_val, x_test, y_val, y_test = split[0], split[1], tuple(split[2::2]), tuple(split[3::2])
                
            else:
                x_train, x_temp, y_train, y_temp = train_test_split(x, y, test_size=split_1, random_state=random_state, stratify=y)
                x_val, x_test, y_val, y_test = train_test_split(x_temp, y_temp, test_size=split_2, random_state=random_state, stratify=y_temp)

            data_class = Sequence if self.data_class == 'auto' else self.data_class
            train = data_class(x_train, y_train, data_type=wfs.data_type.TRAIN, **self.kwargs)
            val = data_class(x_val, y_val, data_type=wfs.data_type.VAL, **self.kwargs)
            test = data_class(x_test, y_test, data_type=wfs.data_type.TEST, **self.kwargs)

            return train, val, test

        raise ValueError('Unknown data type.')

def auto(*args, **kwargs):
    return Auto(*args, **kwargs).out

def build_encode_decode_from_y(y, return_multi_label=False):
    multi_label = isinstance(y, tuple)
    encode, decode = wfs.identity, wfs.identity

    if multi_label:
        encoders = {}
        needs_encoding = False
        for i, y_sub in enumerate(y):
            if np.squeeze(y_sub).ndim == 1:
                needs_encoding = True
                encoder = OneHotEncoder(cols=[0], return_df=False)
                encoder.fit(y_sub)
                encoders[i] = encoder

        if needs_encoding: 
            def encode(y):
                out = []
                for i, y_sub in enumerate(y):
                    if i in encoders:
                        y_sub = encoders[i].transform(y_sub)
                    out.append(y_sub)
                return tuple(out)

            def decode(y_pred):
                out = []
                for i, y_sub in enumerate(y_pred):
                    if i in encoders:
                        y_sub[np.where(y_sub < y_sub.max(axis=1, keepdims=True))] = 0 
                        y_sub = encoders[i].inverse_transform(np.ceil(y_sub)).squeeze()
                    out.append(y_sub)
                return tuple(out)



    elif np.squeeze(y).ndim == 1:
        encoder = OneHotEncoder(cols=[0], return_df=False)
        encoder.fit(y)

        def decode(y_pred):
            y_pred[np.where(y_pred < y_pred.max(axis=1, keepdims=True))] = 0

            return encoder.inverse_transform(np.ceil(y_pred)).squeeze()

        encode =  encoder.transform

    if return_multi_label:
        return multi_label, encode, decode

    return encode, decode