from functools import partial
import tensorflow as tf
import numpy as np

import wfs
from .data import Data, DataType

AUTO = tf.data.experimental.AUTOTUNE

class TFData(Data):
    def __init__(self, x, y=None, **kwargs):
        self._x, self._y = x, y

        self.has_y = self_y is not None
        self.multi_label = isinstance(self._y, tuple)

        self._num_samples = x.shape[0]

        if self.multi_label:
            self._num_classes = tuple(y_sub.shape[1] for y_sub in self._y)
        else:
            self._num_classes = self._y.shape[1]

        self.dataset =  tf.data.Dataset.from_tensor_slices((x, y))

        self.initialize(**kwargs)

    # Set self.dataset, self._num_samples and self._num_classes before calling
    def initialize(self, data_type=DataType.TRAIN, batch_size=32, preprocess_input=wfs.identity, augmentation=None, shuffle_buffer=2048):
        self.data_type = data_type
        self.batch_size = batch_size

        if self.data_type == DataType.TRAIN:
            if augmentation is not None:
                self.dataset = self.dataset.map(augmentation, num_parallel_calls=AUTO)

            self.dataset = self.dataset.shuffle(shuffle_buffer)

        self.dataset = self.dataset.batch(batch_size)
        self.dataset = self.dataset.map(lambda x, y: (preprocess_input(x), y), num_parallel_calls=AUTO)

        if self.data_type == DataType.VAL:
            self.dataset = self.dataset.cache()

        self.dataset = self.dataset.prefetch(AUTO)

    @property
    def x(self):
        labels = self.dataset.map(lambda x, y: x).unbatch().batch(self.num_samples)
        return next(iter(labels)).numpy()

    @property
    def y(self):
        labels = self.dataset.map(lambda x, y: y).unbatch().batch(self.num_samples)
        return next(iter(labels)).numpy()

    @property
    def num_samples(self): 
        return self._num_samples

    @property
    def num_classes(self): 
        return self._num_classes

    @property
    def num_batches(self):
        return np.ceil(self._num_samples / self.batch_size).astype(np.int)

    @property
    def x_shape(self):
        return next(iter(self.dataset.unbatch().take(1)))[0].numpy().shape

    @property
    def out(self):
        return self.dataset

# Specify num_classes to automatically one-hot y
class TFRecordData(TFData):
    def __init__(self, filenames, num_samples, num_classes, ordered=False, **kwargs):
        self.dataset = tf.data.TFRecordDataset(filenames, num_parallel_reads=AUTO)
        self._num_samples = num_samples
        self._num_classes = num_classes

        options = tf.data.Options()
        if not ordered:
            options.experimental_deterministic = False

        self.dataset = self.dataset.with_options(options)
        self.dataset = self.dataset.map(partial(self.tfrecord_read_func, **kwargs), num_parallel_calls=AUTO)

        self.initialize(**kwargs)

    # Potentially a custom y_encode function may accept the num_classes argument
    def tfrecord_read_func(self, example, x_encode='jpeg', y_encode='onehot', y_decode='onehot', x_label='image', y_label='class'):
        tfrecord_format = {
            x_label: tf.io.FixedLenFeature([], tf.string),
            y_label: tf.io.FixedLenFeature([], tf.int64),
        }
        example = tf.io.parse_single_example(example, tfrecord_format)

        if x_encode == 'jpeg':
            x_encode = partial(tf.io.decode_jpeg, channels=3)
        elif x_encode == 'png':
            x_encode = partial(tf.io.decode_png, channels=3)

        if y_encode == 'onehot':
            y_encode = one_hot_encode

        if y_decode == 'onehot':
            y_decode = one_hot_decode

        self.decode = y_decode

        try:
            y_encode = partial(y_encode, num_classes=self.num_classes)
        except TypeError:
            pass

        x = x_encode(example[x_label])
        y = y_encode(example[y_label])

        return x, y

def one_hot_encode(y, num_classes):
    return tf.one_hot(y, num_classes)

def one_hot_decode(y, num_classes):
    return tf.argmax(onehot, axis=1)