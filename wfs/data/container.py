import wfs
from .data import Data

class Container(Data):
    def __init__(self, train=None, val=None, test=None):
        self.train = train
        self.val = val
        self.test = test

    @property
    def has_train(self):
        return self.train is not None

    @property
    def has_val(self):
        return self.val is not None

    @property
    def has_test(self):
        return self.test is not None

    @property
    def has_multiple_datasets(self):
        return (self.has_train and (self.has_val or self.has_test)) or (self.has_val and self.has_test)

    @property
    def x(self):
        if self.has_multiple_datasets:
            raise RuntimeError('Cannot call x on a Container with multiple datasets attached.')

        if self.has_train:
            return self.train.x

        if self.has_val:
            return self.val.x

        if self.has_test:
            return self.test.x

    @property
    def y(self):
        if self.has_multiple_datasets:
            raise RuntimeError('Cannot call y on a Container with multiple datasets attached.')

        if self.has_train:
            return self.train.y

        if self.has_val:
            return self.val.y

        if self.has_test:
            return self.test.y

    @property
    def num_samples(self):
        ret = []
        if self.has_train:
            ret.append(self.train.num_samples)

        if self.has_val:
            ret.append(self.val.num_samples)

        if self.has_test:
            ret.append(self.test.num_samples)

        return tuple(ret)

    @property
    def num_classes(self):
        if self.has_train:
            return self.train.num_classes

        if self.has_val:
            return self.val.num_classes

        raise ValueError('This Container does not have y data.')

    @property
    def x_shape(self):
        if self.has_train:
            return self.train.x_shape

        if self.has_val:
            return self.val.x_shape

        if self.has_test:
            return self.test.x_shape

        raise ValueError('This Container does not have data.')

    @property
    def out(self):
        if self.has_multiple_datasets:
            raise RuntimeError('Cannot call out on a Container with multiple datasets attached.')

        if self.has_train:
            return self.train.out

        if self.has_val:
            return self.val.out

        if self.has_test:
            return self.test.out

    @property
    def y_decode(self):
        if self.has_train:
            return self.train.y_decode

        if self.has_val:
            return self.val.y_decode

        if self.has_test:
            return self.test.y_decode

        raise ValueError('This Container does not have data.')

    @property
    def multi_label(self):
        if self.has_train:
            return self.train.multi_label

        if self.has_val:
            return self.val.multi_label

        if self.has_test:
            return self.test.multi_label

        raise ValueError('This Container does not have data.')