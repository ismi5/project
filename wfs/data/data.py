from abc import ABC, abstractmethod

class Data(ABC):
    splittable = False

    @property
    @abstractmethod
    def x(self): 
        pass

    @property
    @abstractmethod
    def y(self): 
        pass

    @property
    @abstractmethod
    def num_samples(self): 
        pass

    @property
    @abstractmethod
    def num_classes(self): 
        pass

    @property
    @abstractmethod
    def x_shape(self):
        pass

    @property
    @abstractmethod
    def out(self):
        pass

    @property
    @abstractmethod
    def y_decode(self):
        pass

    @property
    @abstractmethod
    def multi_label(self):
        pass
    

