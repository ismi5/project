from functools import partial
import tensorflow as tf
import numpy as np

import wfs
from .data import Data
import wfs.data_type

AUTO = tf.data.experimental.AUTOTUNE

class TFData(Data):
    def __init__(
            self, dataset, num_samples, num_classes=None, x_shape=None, y_decode=None, multi_label=None, data_type=wfs.data_type.TRAIN,
            batch_size=32, preprocess_input=wfs.identity, augmentation=None, shuffle_buffer=2048
        ):
        self.dataset = dataset
        self._num_samples = num_samples
        self._num_classes = num_classes
        self._y_decode = y_decode
        self._multi_label = multi_label
        self.data_type = data_type
        self.batch_size = batch_size

        if self.data_type == wfs.data_type.TRAIN:
            self.dataset = self.dataset.shuffle(shuffle_buffer)

        self.dataset = self.dataset.batch(batch_size)

        if self.data_type == wfs.data_type.TRAIN and augmentation is not None:
            self.dataset = self.dataset.map(augmentation, num_parallel_calls=AUTO)

        self.dataset = self.dataset.map(preprocess_input, num_parallel_calls=AUTO)

        if self.data_type == wfs.data_type.VAL:
            self.dataset = self.dataset.cache()

        self.dataset = self.dataset.prefetch(AUTO)

        self._x_shape = next(iter(self.dataset.unbatch().take(1)))[0].numpy().shape if x_shape is None else x_shape

    @property
    def x(self):
        labels = self.dataset.map(lambda x, y: x).unbatch().batch(self.num_samples)
        return next(iter(labels)).numpy()

    @property
    def y(self):
        labels = self.dataset.map(lambda x, y: y).unbatch().batch(self.num_samples)
        return next(iter(labels)).numpy()

    @property
    def num_samples(self): 
        return self._num_samples

    @property
    def num_classes(self): 
        return self._num_classes

    @property
    def num_batches(self):
        return np.ceil(self._num_samples / self.batch_size).astype(np.int)

    @property
    def x_shape(self):
        return self._x_shape

    @property
    def out(self):
        return self.dataset

    @property
    def y_decode(self):
        return self._y_decode

    @property
    def multi_label(self):
        return self._multi_label

    # y should be one-hot encoded already
    # do not call directly but use through wfs.data.auto(data_class=wfs.data.TFData.from_numpy)
    def from_numpy(x, y=None, y_decode=None, multi_label=False, data_type=wfs.data_type.TRAIN, x_shape=None, **kwargs):
        num_samples = x.shape[0]

        if y is None:
            dataset =  tf.data.Dataset.from_tensor_slices(x)
            return TFData(dataset, data_type, num_samples, **kwargs)
        
        if y_decode is None:
            y_decode = wfs.identity

        if multi_label:
            num_classes = tuple(y_sub.shape[1] for y_sub in y)
        else:
            num_classes = y.shape[1]

        dataset =  tf.data.Dataset.from_tensor_slices((x, y))

        return TFData(dataset, num_samples, num_classes, x_shape, y_decode, multi_label, data_type, **kwargs)

    # x_shape is neccesary for execution on TPU
    def from_tfrecords(
            filenames, num_samples, num_classes, multi_label=False, data_type=wfs.data_type.TRAIN, ordered=False, x_label='image',
            y_label='class', x_encode=None, x_shape=None, y_encode=None, y_decode=None, **kwargs
        ):

        dataset = tf.data.TFRecordDataset(filenames, num_parallel_reads=AUTO)

        options = tf.data.Options()
        if not ordered:
            options.experimental_deterministic = False

        dataset = dataset.with_options(options)

        if x_encode is None or x_encode == 'none':
            x_encode = wfs.identity
        elif x_encode == 'jpeg':
            x_encode = partial(tf.io.decode_jpeg, channels=3)
        elif x_encode == 'png':
            x_encode = partial(tf.io.decode_png, channels=3)

        if y_encode is None or y_encode == 'none':
            y_encode = wfs.identity
        else:
            if y_encode == 'onehot':
                y_encode = one_hot_encode

            y_encode = partial(y_encode, num_classes)

        if y_decode is None or y_decode == 'none':
            y_decode = wfs.identity
        else:
            if y_decode == 'onehot':
                y_decode = one_hot_decode

            y_decode = partial(y_decode, num_classes, multi_label)

        dataset = dataset.map(partial(tfrecord_read_func, num_classes, x_label, y_label, multi_label, x_encode, x_shape, y_encode), num_parallel_calls=AUTO)

        return TFData(dataset, num_samples, num_classes, x_shape, y_decode, multi_label, data_type, **kwargs)

def tfrecord_read_func(num_classes, x_label, y_label, multi_label, x_encode, x_shape, y_encode, example):
    tfrecord_format = {
        x_label: tf.io.FixedLenFeature([], tf.string),
    }
    if multi_label:
        for y_label_sub in y_label:
            tfrecord_format[y_label_sub] = tf.io.FixedLenFeature([], tf.int64)
    else:
        tfrecord_format[y_label] = tf.io.FixedLenFeature([], tf.int64)

    example = tf.io.parse_single_example(example, tfrecord_format)
    x = x_encode(example[x_label])

    if x_shape is not None:
        x = tf.reshape(x, x_shape)

    if multi_label:
        y = []
        for y_label_sub in y_label:
            y.append(y_encode(example[y_label_sub]))

    else:
        y = y_encode(example[y_label])
        
    return x, y

def one_hot_encode(num_classes, y):
    return tf.one_hot(y, num_classes)

def one_hot_decode(num_classes, multi_label, y):
    if multi_label:
        out = []
        for y_sub in y:
            out.append(tf.argmax(y_sub, axis=1))

        return tuple(out)

    return tf.argmax(y, axis=1)