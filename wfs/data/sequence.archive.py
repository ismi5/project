import tensorflow as tf
import numpy as np

import wfs
import wfs.data_type
from .data import Data

# y should be one-hot encoded already
class Sequence(Data, tf.keras.utils.Sequence):
    def __init__(self, x, y=None, y_decode=None, multi_label=None, data_type=wfs.data_type.TRAIN, batch_size=32, preprocess_input=wfs.identity, augmentation=None, mixup=0, **kwargs):
        self._num_samples = x.shape[0]
        self._x, self._y = x, y
        self.has_y = self._y is not None
        if self.has_y:
            if y_decode is None:
                ValueError('Must pass y_decode if y is given.')

            if multi_label is None:
                ValueError('Must pass multi_label if y is given.')

        self._y_decode = y_decode
        self._multi_label = multi_label
        self.data_type = data_type
        self.batch_size = batch_size
        self.preprocess_input = preprocess_input

        self.augmentation = augmentation if self.data_type == wfs.data_type.TRAIN else None
        self.mixup = mixup # todo should remove
        
        
        self.multi_label = isinstance(self._y, tuple)

        self.on_epoch_end()

    def __len__(self):
        return self.num_batches

    @property
    def x(self):
        return self._x[self.indexes]

    @property
    def y(self):
        return self._y[self.indexes]

    @property
    def num_samples(self):
        return self._num_samples

    @property
    def num_classes(self):
        if self.multi_label:
            return tuple(y_sub.shape[1] for y_sub in self._y)
        return self._y.shape[1]

    @property
    def num_batches(self):
        return np.ceil(self._num_samples / self.batch_size).astype(np.int)

    @property      
    def x_shape(self):
        return self._x.shape[1:]

    @property
    def out(self):
        return self

    @property
    def y_decode(self):
        return self._y_decode

    @property
    def multi_label(self):
        return self._multi_label

    def __getitem__(self, idx):
        idxs = self.indexes[idx * self.batch_size:(idx + 1) * self.batch_size]
        
        # Generate x_batch
        x_batch = self._x[idxs]

        if self.augmentation is not None:
            x_batch = self.augmentation(x_batch)
            
        if self.mixup > 0:
            lam = np.random.beta(self.mixup, self.mixup)
            
            perms = np.random.permutation(idxs.size)
            
            x_batch = lam * x_batch + (1 - lam) * x_batch[perms]

        x_batch = self.preprocess_input(x_batch)
        
        if not self.has_y:
            return x_batch

        # Generate y_batch
        if self.multi_label:
            y_batch = tuple(y_sub[idxs] for y_sub in self._y)
        else:
            y_batch = self._y[idxs]

        if self.mixup > 0:
            if self.multi_label:
                y_batch = tuple(lam * y_batch_sub + (1 - lam) * y_batch_sub[perms] for y_batch_sub in y_batch)
            else:
                y_batch = lam * y_batch + (1 - lam) * y_batch[perms]

        return x_batch, y_batch

    def on_epoch_end(self):
        self.indexes = np.arange(self._num_samples)
        np.random.shuffle(self.indexes)