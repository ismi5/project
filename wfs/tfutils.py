import sys
import tensorflow as tf

import wfs

tf.get_logger().setLevel('ERROR')

wfs.tfstrategy = tf.distribute.get_strategy()
wfs.tfdevices = 1

def enable_tpu(fail_hard=False):
    try:
        tpu = tf.distribute.cluster_resolver.TPUClusterResolver()
        tf.config.experimental_connect_to_cluster(tpu)
        tf.tpu.experimental.initialize_tpu_system(tpu)
        wfs.tfstrategy = tf.distribute.experimental.TPUStrategy(tpu)
        wfs.tfdevices = wfs.tfstrategy.num_replicas_in_sync

        print('Running on TPU %s with %d replicas.' % (tpu.master(), wfs.tfdevices))
    except ValueError:
        if fail_hard:
            raise RuntimeError('Could not initialize TPU.')
        print('Could not initialize TPU.', file=sys.stderr)

expand_dims_orig = tf.expand_dims

def expand_dims(input, axis=None, axes=None, name=None):
    if axis is not None:
        return expand_dims_orig(input, axis, name)
    
    for axis in axes:
        input = expand_dims_orig(input, axis, name)
        
    return input

tf.expand_dims = expand_dims