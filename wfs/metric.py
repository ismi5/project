from functools import partial
import sklearn.metrics as skm

METRIC_DEFINITIONS = {
    'accuracy': skm.accuracy_score,
    'acc': skm.accuracy_score,

    'auc': skm.roc_auc_score,
    'roc_auc': skm.roc_auc_score,

    'f1': skm.f1_score,

    'recall': skm.recall_score,
    'precision': skm.precision_score,

    'cohen_kappa': skm.cohen_kappa_score,
    'dcg': skm.dcg_score,
    'ndcg': skm.ndcg_score,
    'jaccard': skm.jaccard_score,
}

METRIC_WITH_AVERAGE = [
    'auc', 'roc_auc', 'f1', 'recall', 'precision'
]

class Metric():
    def __init__(self, mixed, label=None, **kwargs):
        if isinstance(mixed, str):
            # Build for known metrics
            split = mixed.lower().replace('_score', '').split(':')
            
            str_func = split[0]
            str_avg = split[1] if len(split) == 2 else 'macro'

            if str_func in METRIC_WITH_AVERAGE:
                self.label = label if label is not None else '%s_%s' % (str_avg, str_func)
                self.metric_fn = partial(METRIC_DEFINITIONS[str_func], average=str_avg, **kwargs)
                return

            self.label = label if label is not None else str_func
            self.metric_fn = partial(METRIC_DEFINITIONS[str_func], **kwargs)
            return

        self.label = label if label is not None else 'metric_score'
        self.metric_fn = partial(mixed, **kwargs)

    def __call__(self, y_true, y_pred):
        return self.metric_fn(y_true, y_pred)