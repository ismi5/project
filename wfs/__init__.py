identity = lambda x: x

from . import data_type
from . import tfutils
from . import data
from . import logger
from . import trainer
from . import metric
from . import augmentation