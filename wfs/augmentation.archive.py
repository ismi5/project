from abc import ABC, abstractmethod
import tensorflow as tf

from . import image_type


class Augmentation(ABC):
    #batch_method is either 'collective' or 'aggregated'
    def __init__(self, p, batch_method=None):
        self.p = tf.convert_to_tensor(p, dtype=tf.float32)

        if p == 1:
            self.apply_with_chance = self.apply
            self.apply_batch_with_chance = self.apply_batch

        else:
            self.apply_with_chance = self._apply_with_chance

            if batch_method == 'collective':
                self.apply_batch_with_chance = self.apply_batch_with_chance_collective

            elif batch_method == 'aggregated':
                self.apply_batch_with_chance = self.apply_batch_with_chance_aggregated

            else:
                raise ValueError('Unknown batch_method %s.' % batch_method)

    def __call__(self, x, y):
        return tf.cond(
            tf.math.equal(x.shape.ndims, 4),
            lambda: self.apply_batch_with_chance(x, y),
            lambda: self.apply_with_chance(x, y)
        )

    def _apply_with_chance(self, x, y):
        return tf.cond(
            tf.math.less(tf.random.uniform([], dtype=tf.float32), self.p),
            lambda: self.apply(x, y),
            lambda: x, y
        )

    @abstractmethod
    def apply(self, x, y):
        pass

    def apply_batch_with_chance_collective(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]

        indices = tf.math.less(tf.reshape(tf.random.uniform([batch_size]), [batch_size, 1, 1, 1]), self.p)
        x_indices = tf.cast(indices, x_batch.dtype)
        y_indices = tf.cast(indices, y_batch.dtype)

        x_batch_applied, y_batch_applied = self.apply_batch(x_batch, y_batch)

        return x_indices * x_batch_applied + (1 - x_indices) * x_batch, y_indices * y_batch_applied + (1 - y_indices) * y_batch

    def apply_batch_with_chance_aggregated(self, x_batch, y_batch):
        return tf.map_fn(self._apply_with_chance, x_batch, y_batch)

    def apply_batch(self, x_batch, y_batch):
        return tf.map_fn(self.apply, x_batch, y_batch)

class Either(Augmentation):
    def __init__(self, augmentation_1, augmentation_2, p=1, batch_method='collective'):
        super().__init__(p, batch_method)
        self.augmentation_1 = augmentation_1
        self.augmentation_2 = augmentation_2

        self.chance_on_augmentation_1 = augmentation_1.p / (augmentation_1.p + augmentation_2.p)

    def apply_batch(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]

        indices = tf.math.less(tf.reshape(tf.random.uniform([batch_size]), [batch_size, 1, 1, 1]), self.chance_on_augmentation_1)
        x_indices = tf.cast(indices, x_batch.dtype)
        y_indices = tf.cast(indices, y_batch.dtype)

        x_batch_1, y_batch_1 = self.augmentation_1.apply_batch(x_batch, y_batch)
        x_batch_2, y_batch_2 = self.augmentation_2.apply_batch(x_batch, y_batch)

        return x_indices * x_batch_1 + (1 - x_indices) * x_batch_2, y_indices * y_batch_1 + (1 - y_indices) * y_batch_2

    def apply(self, x, y):
        return tf.cond(
            tf.math.less(tf.random.uniform([], dtype=tf.float32), self.chance_on_augmentation_1),
            lambda: self.augmentation_1.apply(x, y),
            lambda: self.augmentation_2.apply(x, y),
        )

class OneOf(Augmentation):
    def __init__(self, *args, p=1, batch_method='collective'):
        super().__init__(p, batch_method)
        if isinstance(args[0], list):
            self.augmentations = args[0]
        else:
            self.augmentations = args

        self.ps = tf.convert_to_tensor([augmentation.p for augmentation in self.augmentations], dtype=tf.float32)
        self.ps /= tf.math.reduce_sum(ps)

        self._random = tf.Variable(0, shape=[], dtype=tf.float32)

    def apply_batch(self, x_batch, y_batch):
        batch_size = tf.shape(x_batch)[0]
        randoms = tf.reshape(tf.random.uniform([batch_size]), [batch_size, 1, 1, 1])

        x_out = tf.zeros_like(x_batch)
        y_out = tf.zeros_like(y_batch)

        old_indices = tf.zeros_like(randoms, dtype=tf.bool)

        for p, augmentation in zip(self.ps, self.augmentations):
            new_indices = tf.math.less(randoms, p)
            new_indices = tf.math.logical_and(new_indices, tf.math.logical_xor(new_indices, old_indices))

            x_indices = tf.cast(new_indices, x_batch.dtype)
            y_indices = tf.cast(new_indices, y_batch.dtype)

            x_batch_applied, y_batch_applied = augmentation.apply_batch(x_batch, y_batch)

            x_out += x_indices * x_batch_applied + (1 - x_indices) * x_batch
            y_out += y_indices * y_batch_applied + (1 - y_indices) * y_batch

            old_indices = new_indices

        return x_out, y_out

    def apply(self, x, y):
        self._random.assign(tf.random.uniform([], dtype=tf.float32))

        return self.apply_helper(x, y, i=tf.zeros([], dtype=tf.int32))

    def apply_helper(self, x, y, i):
        return tf.cond(
            tf.math.equal(i, self.length),
            lambda: self.augmentations[i].apply(x, y),
            lambda: tf.cond(
                tf.math.less(self._random, tf.math.reduce_sum(self.ps[:i])),
                lambda: self.augmentations[i].apply(x, y),
                lambda: self.apply_helper(x, y, i + 1)
            )
        )

class Sequence(Augmentation):
    def __init__(self, *args, p=1, batch_method='collective'):
        super().__init__(p, batch_method)
        if isinstance(args[0], list):
            self.augmentations = args[0]
        else:
            self.augmentations = args

    def apply_batch(self, x_batch, y_batch):
        for augmentation in self.augmentations:
            x_batch, y_batch = augmentation.apply_batch_with_chance(x_batch, y_batch)

        return x_batch, y_batch

    def apply(self, x, y):
        for augmentation in self.augmentations:
            x, y = augmentation.apply_with_chance(x, y)

        return x, y

class CastX(Augmentation):
    def __init__(self, x_dtype):
        super().__init__(1)
        self.x_dtype = x_dtype

    def __call__(self, x, y):
        return tf.cast(x, self.x_dtype), y

    def apply_batch(self, x_batch, y_batch):
        return self(x_batch, y_batch)

    def apply(self, x, y):
        return self(x, y)

class CastY(Augmentation):
    def __init__(self, y_dtype):
        super().__init__(1)
        self.y_dtype = y_dtype

    def __call__(self, x, y):
        return x, tf.cast(y, self.y_dtype)

    def apply_batch(self, x_batch, y_batch):
        return self(x_batch, y_batch)

    def apply(self, x, y):
        return self(x, y)

class CastXY(Augmentation):
    def __init__(self, x_dtype, y_dtype):
        super().__init__(1)
        self.x_dtype = x_dtype
        self.y_dtype = y_dtype

    def __call__(self, x, y):
        return tf.cast(x, self.x_dtype), y

    def apply_batch(self, x_batch, y_batch):
        return self(x_batch, y_batch)

    def apply(self, x, y):
        return self(x, y)

class HorizontalFlip(Augmentation):
    def __init__(self, p=0.5):
        super().__init__(p, batch_method='collective')

    def apply_batch(self, x_batch, y_batch):
        return tf.reverse(x_batch, [2]), y_batch

    def apply(self, x, y):
        return tf.reverse(x, [1]), y

class VerticalFlip(Augmentation):
    def __init__(self, p=0.5):
        super().__init__(p, batch_method='collective')

    def apply_batch(self, x_batch, y_batch):
        return tf.reverse(x_batch, [1]), y_batch

    def apply(self, x, y):
        return tf.reverse(x, [0]), y

# Cast to float32 using CastX(tf.float32) before using
class Brightness(Augmentation):
    def __init__(self, ratio, image_type=image_type.UINT0_255, p=1):
        super().__init__(p, batch_method='collective')
        self.max_delta = ratio * image_type.range()
        self.image_type = image_type

    def apply_batch(self, x_batch, y_batch):
        shape = tf.shape(x_batch)
        deltas = tf.random.uniform((shape[0], 1, 1, 1), -self.max_delta, self.max_delta, dtype=tf.float32)

        tiler = tf.concat([[1], tf.shape(x_batch)[1:]], axis=0)
        deltas = tf.tile(deltas, tiler)

        x_batch = self.image_type.clip(tf.math.add(x_batch, deltas))

        return x_batch, y_batch

    def apply(self, x, y):
        delta = tf.random.uniform([], -self.max_delta, self.max_delta, dtype=tf.float32)

        x = self.image_type.clip(tf.math.add(x, delta)), y
        x = tf.cast(x, original_dtype)

        return x, y

# Cast to float32 using CastX(tf.float32) before using
# class Contrast(Augmentation):
#     def __init__(self, max_factor, image_type=image_type.UINT0_255, p=0.5):
#         super().__init__(p, batch_method='collective')
#         self.max_factor = max_factor
#         self.image_type = image_type

#     def apply_batch(self, x_batch, y_batch):
#         batch_size = tf.shape(x_batch)[0]
#         deltas = tf.random.uniform([batch_size], -self.max_delta, self.max_delta dtype=tf.float32)

#         original_dtype = x.dtype
#         x_batch = tf.cast(x_batch, tf.float32)
#         x_batch = self.image_type.clip(tf.math.add(x_batch, deltas))
#         x_batch = tf.cast(x_batch, original_dtype)

#         return x_batch, y_batch

#     def apply(self, x, y):
#         delta = tf.random.uniform([], -self.max_delta, self.max_delta, dtype=tf.float32)

#         x = self.image_type.clip(tf.math.multiply(x, delta)), y

#         return x, y
