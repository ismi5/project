import tensorflow as tf

import wfs
from .backbonetrainer import BackboneTrainer

# Automatically builds a classification model for the target training data
# this function supports multilabel data, just pass a tuple to y
# use validation_split=None to not create validation data
# metric should be a wfs.metric.Metric
class ImageClassification(BackboneTrainer):
    def _init_model(self, weights, dropout_rate, **kwargs):
        with wfs.tfstrategy.scope():
            base = self.backbone.build_model(input_shape=self.data.x_shape, weights=weights, **kwargs)
            out = tf.keras.layers.GlobalAveragePooling2D(name='top_pool')(base.output)

            if dropout_rate == 'default':
                dropout_rate = self.backbone.default_dropout_rate
            if dropout_rate is not None:
                out = tf.keras.layers.Dropout(dropout_rate, name='top_dropout')(out)

            if self.data.multi_label:
                output = []
                for i, num_classes_sub in enumerate(self.data.num_classes):
                    output.append(tf.keras.layers.Dense(num_classes_sub, activation='softmax', kernel_initializer=self.backbone.kernel_initializer, name='output%i' % i)(out))
            else:
                output = tf.keras.layers.Dense(self.data.num_classes, activation='softmax', kernel_initializer=self.backbone.kernel_initializer, name='output')(out)

            self.model = tf.keras.models.Model(base.input, output)
