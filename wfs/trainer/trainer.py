from abc import ABC, abstractmethod

class Trainer(ABC):
    @abstractmethod
    def train(self, **kwargs):
        pass

    @abstractmethod
    def validate(self):
        pass

    @abstractmethod
    def evaluate_test(self):
        pass

    @abstractmethod
    def predict(self, data):
        pass