from abc import ABC, abstractmethod

import wfs
from .trainer import Trainer
from .classification_backbones import CLASSIFICATION_BACKBONES

class BackboneTrainer(Trainer):
    def __init__(self, *args, backbone='efficientnet-b0', validation_metric='accuracy', additional_metrics=[], loss='categorical_crossentropy',
            weights=None, dropout_rate='default', learning_rate='default', additional_callbacks=[], **kwargs):

        self.backbone = CLASSIFICATION_BACKBONES[backbone.lower()] if isinstance(backbone, str) else backbone

        self.data = args[0] if isinstance(*args, wfs.data.Container) else wfs.data.auto(*args, **kwargs)
        self.validation_metric = validation_metric if isinstance(validation_metric, wfs.metric.Metric) else wfs.metric.Metric(validation_metric)

        self.additional_metrics = []
        for additional_metric in additional_metrics:
            self.additional_metrics.append(additional_metric if isinstance(additional_metric, wfs.metric.Metric) else wfs.metric.Metric(additional_metric))

        self.all_metrics = [self.validation_metric] + self.additional_metrics
        
        self._init_model(weights, dropout_rate, **kwargs)

        self.optimizer = self.backbone.build_optimizer(learning_rate, self.data.train, **kwargs)
        self.model.compile(self.optimizer, loss=loss)
        self.loss = self.model.loss
        self.logger = wfs.logger.Keras(self, **kwargs)
        
        self.callbacks = additional_callbacks
        self.callbacks.append(self.logger.attach(self.data.train))

    def train(self, epochs=1):
        self.model.fit(self.data.train.out, shuffle=False, epochs=epochs, verbose=0,
            callbacks=self.callbacks)

    def validate(self):
        y_pred = self.predict(self.data.val)
        y_true = self.data.y_decode(self.data.val.y)

        metric_results = [self.validation_metric(y_true, y_pred)]
        for additional_metric in self.additional_metrics:
            metric_results.append(additional_metric(y_true, y_pred))

        return self.loss(y_true, y_pred), metric_results 

    def evaluate_test(self):
        y_pred = self.predict(self.data.test)
        y_true = self.data.y_decode(self.data.test.y)

        metric_results = [self.validation_metric(y_true, y_pred)]
        for additional_metric in self.additional_metrics:
            metric_results.append(additional_metric(y_true, y_pred))

        return self.loss(y_true, y_pred), metric_results

    def predict(self, data):
        predictions = self.model.predict(data.out, verbose=0, callbacks=[self.logger.attach(data)])

        return self.data.y_decode(predictions)

    @abstractmethod
    def  _init_model(self, weights, dropout_rate, learning_rate, **kwargs):
        pass