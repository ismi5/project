from .efficientnet import EfficientNet
from .keras_models import Factory, KerasModel

CLASSIFICATION_BACKBONES = {
    'efficientnet-b0': EfficientNet(1.0, 1.0, 224, 0.2, 'efficientnet-b0'),
    'efficientnet-b1': EfficientNet(1.0, 1.1, 240, 0.2, 'efficientnet-b1'),
    'efficientnet-b2': EfficientNet(1.1, 1.2, 260, 0.3, 'efficientnet-b2'),
    'efficientnet-b3': EfficientNet(1.2, 1.4, 300, 0.3, 'efficientnet-b3'),
    'efficientnet-b4': EfficientNet(1.4, 1.8, 380, 0.3, 'efficientnet-b4'),
    'efficientnet-b5': EfficientNet(1.6, 2.2, 456, 0.4, 'efficientnet-b5'),
    'efficientnet-b6': EfficientNet(1.8, 2.6, 528, 0.5, 'efficientnet-b6'),
    'efficientnet-b7': EfficientNet(2.0, 3.1, 600, 0.5, 'efficientnet-b7'),
    'efficientnet-l2': EfficientNet(4.3, 5.3, 800, 0.5, 'efficientnet-l2'),
}

for key in Factory.models_names:
    CLASSIFICATION_BACKBONES[key] = KerasModel(key)