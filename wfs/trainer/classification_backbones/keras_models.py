import functools
import tensorflow as tf
import keras_applications as ka
from .backbone import ClassificationBackbone

# This module contains a lot of models, however the optimizer and other parameters do not have correct default values
# In an ideal situation this module would not exist and instead all models below have their separate module
# This is actually a soft todo, however it would probably only be relevant for 'good' models

class ModelsFactory:
    _models = {

        # ResNets
        'resnet50': [ka.resnet.ResNet50, ka.resnet.preprocess_input],
        'resnet101': [ka.resnet.ResNet101, ka.resnet.preprocess_input],
        'resnet152': [ka.resnet.ResNet152, ka.resnet.preprocess_input],

        # Resnet V2
        'resnet50v2': [ka.resnet_v2.ResNet50V2, ka.resnet_v2.preprocess_input],
        'resnet101v2': [ka.resnet_v2.ResNet101V2, ka.resnet_v2.preprocess_input],
        'resnet152v2': [ka.resnet_v2.ResNet152V2, ka.resnet_v2.preprocess_input],

        # ResNext
        'resnext50': [ka.resnext.ResNeXt50, ka.resnext.preprocess_input],
        'resnext101': [ka.resnext.ResNeXt101, ka.resnext.preprocess_input],

        # VGG
        'vgg16': [ka.vgg16.VGG16, ka.vgg16.preprocess_input],
        'vgg19': [ka.vgg19.VGG19, ka.vgg19.preprocess_input],

        # Densnet
        'densenet121': [ka.densenet.DenseNet121, ka.densenet.preprocess_input],
        'densenet169': [ka.densenet.DenseNet169, ka.densenet.preprocess_input],
        'densenet201': [ka.densenet.DenseNet201, ka.densenet.preprocess_input],

        # Inception
        'inceptionresnetv2': [ka.inception_resnet_v2.InceptionResNetV2,
                              ka.inception_resnet_v2.preprocess_input],
        'inceptionv3': [ka.inception_v3.InceptionV3, ka.inception_v3.preprocess_input],
        'xception': [ka.xception.Xception, ka.xception.preprocess_input],

        # Nasnet
        'nasnetlarge': [ka.nasnet.NASNetLarge, ka.nasnet.preprocess_input],
        'nasnetmobile': [ka.nasnet.NASNetMobile, ka.nasnet.preprocess_input],

        # MobileNet
        'mobilenet': [ka.mobilenet.MobileNet, ka.mobilenet.preprocess_input],
        'mobilenetv2': [ka.mobilenet_v2.MobileNetV2, ka.mobilenet_v2.preprocess_input], 
    }

    @property
    def models(self):
        return self._models

    @property
    def models_names(self):
        return list(self.models.keys())

    @staticmethod
    def get_kwargs():
        return {
            'backend': tf.keras.backend,
            'layers': tf.keras.layers,
            'models': tf.keras.models,
            'utils': tf.keras.utils,
        }

    def inject_submodules(self, func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            modules_kwargs = self.get_kwargs()
            new_kwargs = dict(list(kwargs.items()) + list(modules_kwargs.items()))
            return func(*args, **new_kwargs)

        return wrapper

    def get(self, name):
        if name not in self.models_names:
            raise ValueError('No such model `{}`, available models: {}'.format(
                name, list(self.models_names)))

        model_fn, preprocess_input = self.models[name]
        model_fn = self.inject_submodules(model_fn)
        preprocess_input = self.inject_submodules(preprocess_input)
        return model_fn, preprocess_input

Factory = ModelsFactory()

class KerasModel(ClassificationBackbone):
    def __init__(self, backbone):
        self._builder, self._preprocess_input = Factory.get(backbone)

    @property
    def default_dropout_rate(self): 
        return None

    @property
    def kernel_initializer(self): 
        return 'glorot_uniform'

    def build_optimizer(self, learning_rate, sequence, **kwargs):
        if learning_rate == 'default':
            learning_rate = 1e-3

        return tf.keras.optimizers.Adam(learning_rate)

    def build_model(self, **kwargs):
        return self._builder(
            include_top=False,
            **kwargs
        )