from abc import ABC, abstractmethod

class ClassificationBackbone(ABC):
    @property
    @abstractmethod
    def default_dropout_rate(self): 
        pass

    @property
    @abstractmethod
    def kernel_initializer(self): 
        pass

    @abstractmethod
    def build_optimizer(self, learning_rate, sequence, **kwargs):
        pass    

    @abstractmethod
    def build_model(self, input_shape, **kwargs):
        pass

