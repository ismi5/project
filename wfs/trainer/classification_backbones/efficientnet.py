import tensorflow as tf
import numpy as np
from .backbone import ClassificationBackbone
import wfs.models.efficientnet as efn 

class EfficientNet(ClassificationBackbone):
    def __init__(self, width_coefficient, depth_coefficient, default_resolution, default_dropout_rate, model_name):
        self.width_coefficient = width_coefficient
        self.depth_coefficient = depth_coefficient
        self.default_resolution = default_resolution
        self.model_name = model_name

        self._default_dropout_rate = default_dropout_rate

    @property
    def default_dropout_rate(self): 
        return self._default_dropout_rate

    @property
    def kernel_initializer(self): 
        return efn.DENSE_KERNEL_INITIALIZER

    def build_optimizer(self, learning_rate, sequence, **kwargs):
        learning_rate = 0.256 / 4096 * sequence.batch_size if learning_rate == 'default' else learning_rate
        learning_rate_schedule = LearningRateSchedule(learning_rate, sequence.num_batches, **kwargs)

        return tf.keras.optimizers.RMSprop(learning_rate_schedule, rho=0.9, momentum=0.9)
    

    def build_model(self, **kwargs):
        return efn.EfficientNet(
            self.width_coefficient,
            self.depth_coefficient,
            self.default_resolution,
            model_name=self.model_name,
            include_top=False,
            pooling=None,
            **kwargs
        )

# First warmup_epochs epochs linear increase from zero to learning_rate
# Then exponential decay every decay_epochs epochs with factor decay_rate
class LearningRateSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(self, learning_rate, steps_per_epoch, decay_rate=0.97, decay_epochs=2.4, warmup_epochs=5, **kwargs):
        super(LearningRateSchedule, self).__init__()
        
        self.learning_rate = learning_rate
        self.steps_per_epoch = steps_per_epoch
        self.decay_rate = decay_rate
        self.decay_epochs = decay_epochs
        self.warmup_epochs = warmup_epochs
        
        self.learning_rate_tensor = tf.convert_to_tensor(learning_rate, tf.float32)
        
        self.decay_steps = np.round(decay_epochs * steps_per_epoch).astype(np.int)
        self.linear_split_point = tf.convert_to_tensor(warmup_epochs * steps_per_epoch, tf.float32)
        
        self.decay_rate_tensor = tf.convert_to_tensor(decay_rate, tf.float32)
        
    @tf.function   
    def __call__(self, step):
        step = tf.cast(step, tf.float32)
        
        if step < self.linear_split_point:
            return tf.convert_to_tensor(self.learning_rate_tensor * (step / self.linear_split_point), tf.float32)
        
        return tf.cast(self.learning_rate * tf.math.pow(self.decay_rate, tf.cast(tf.math.floor((step - self.linear_split_point) / self.decay_steps), tf.float32)), dtype=tf.float32)
        
        
    def get_config(self):
        return {
            'learning_rate': self.learning_rate,
            'steps_per_epoch': self.steps_per_epoch,
            'decay_rate': self.decay_rate,
            'decay_epochs': self.decay_epochs,
            'warmup_epochs': self.warmup_epochs
        }